package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.UIManager;

import logic.HybridMap;
import logic.MapManager;
import logic.PolygonLayer;
import logic.Sprite;
import logic.SpriteLayer;
import logic.TiledLayer;
import logic.TiledMap;
import data.ImageLoader;
import data.MapExporter;
import data.MapExporter.IOType;
import data.ScriptInterpreter;
import data.SpriteSet;
import data.TileSet;

/**
 * The ViewController is meant to be an interface between the MapManager and
 * EditorView classes. It contains instances of both classes and acts as a
 * listener in each class. By using the public fireEvent method and the events
 * that are also defined in the ViewController class, the objects may call upon
 * the ViewController to perform changes.
 */
public class ViewController implements KeyListener {
	/**
	 * The MapEvent enums are used to specify the type of event that is fired
	 * when the fireEvent method is called.
	 */
	public enum MapEvent {
		// Map
		NEWHYBRIDMAP, CHANGELAYER, ADDLAYER, REMOVELAYER, SAVEMAP, OPENMAP, IMPORTMAP, EXPORTMAP, RESIZEMAP, PACKRESIZEMAP,

		// View
		UPDATEINFO, UPDATEVIEW, SETSCALE, SETPOLYTHICKNESS, SETBACKXOR, BACKGROUNDCOLOR, POLYGONCOLOR, SPRITECOLOR, ONIONSKIN, GRID, BACKDARKENED, CHANGEXOFFSET, CHANGEYOFFSET,

		//Edit
		EDITMODE, SELECTMODE,
		
		// Tile
		SETTILE, UNDO, REDO, ADDBRUSH, SETBRUSH, SELECTION, ADDTILESET, SETTILESET, SETCURRENTTILE,

		// Sprite
		SETCURRENTSPRITE, ADDSPRITE, REMOVESPRITE, CHANGESPRITE, SETSPRITESET, ADDSPRITESET,

		// Polygon
		ADDPOINT, MOVEPOINT, INFLATE, REMOVEPOINT, ADDPOLYGON, REMOVEPOLYGON, CHANGEPOLYGON, SELECTPOINT
	};

	/**
	 * ViewController keeps the main instances of the GUI objects and handles
	 * events, as specified in the {@link ViewController.MapEvent} enum between
	 * these instances.
	 */
	private MapManager mapManager;
	private EditorView editorView;
	private EditPanel editPanel;
	private JPanel mapControlPanel;
	private TilePanel tilePanel;
	private ToolPanel toolPanel;
	private PolyPanel polyPanel;
	private SpritePanel spritePanel;
	private HashMap<String, TileSet> tileSets;
	private LinkedHashMap<String, SpriteSet> spriteSets;

	/**
	 * Constructor. Instantiates both the mapManager and the editorView
	 * objects..
	 */
	public ViewController() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		spriteSets = new LinkedHashMap<String, SpriteSet>(0);
		tileSets = new HashMap<String, TileSet>(0);

		mapManager = new MapManager(this);
		mapManager.SetWorkingDirectory(System.getProperty("user.dir"));
		MapExporter.SetWorkingDirectory(System.getProperty("user.dir"));
		ImageLoader.SetWorkingDirectory(System.getProperty("user.dir"));

		editorView = new EditorView(this);

		mapControlPanel = new JPanel(new GridLayout(1, 1));
		mapControlPanel.setPreferredSize(new Dimension(
				editorView.getWidth() / 4, editorView.getHeight()));
		editorView.getContentPane().add(mapControlPanel, BorderLayout.EAST);

		initCenterPanel();
		initToolPanel();
		initTilePanel();
		initPolyPanel();
		initSpritePanel();

		editorView.validate();
	}

	/**
	 * Initializes the center panel.
	 */
	private void initCenterPanel() {
		editPanel = new EditPanel(this, editorView.getWidth(),
				editorView.getHeight());
		editPanel.getMapCanvas().setSpriteSets(spriteSets);
		editPanel.getMapCanvas().setTileSets(tileSets);
		editorView.getContentPane().add(editPanel, BorderLayout.CENTER);
	}

	/**
	 * Initializes the tool panel.
	 */
	private void initToolPanel() {
		toolPanel = new ToolPanel(this, editorView.getWidth(),
				editorView.getHeight());
		editorView.getContentPane().add(toolPanel, BorderLayout.WEST);
	}

	/**
	 * Initializes the poly panel.
	 */
	private void initPolyPanel() {
		polyPanel = new PolyPanel(this, editorView.getWidth(),
				editorView.getHeight());
		polyPanel.setPreferredSize(new Dimension(editorView.getWidth() / 4,
				editorView.getHeight()));
	}

	/**
	 * Initializes the tile panel.
	 */
	private void initTilePanel() {
		tilePanel = new TilePanel(this, editorView.getWidth(),
				editorView.getHeight());
		tilePanel.setPreferredSize(new Dimension(editorView.getWidth() / 4,
				editorView.getHeight()));
	}

	/**
	 * Initializes the sprite panel.
	 */
	private void initSpritePanel() {
		spritePanel = new SpritePanel(this, editorView.getWidth(),
				editorView.getHeight());
		spritePanel.setPreferredSize(new Dimension(editorView.getWidth() / 4,
				editorView.getHeight()));
	}

	/**
	 * The fireEvent method is used by classes that use ViewController as a
	 * listener whenever they wish to have the ViewController change something.
	 * See MapEvent for more specific information about types of events.
	 * 
	 * @param me
	 *            - MapEvent type that specifies the kind of event to be fired.
	 * @param args
	 *            - An integer array that contains parameters for the specific
	 *            type of MapEvent.
	 */
	public void fireEvent(MapEvent me, int[] args) {

		switch (me) {
		case NEWHYBRIDMAP:
			args = newMapPrompt();
			if (args != null) {
				mapManager.newHybridMap(args[0], args[1], args[2], args[3]);

				if (mapManager.getMap() != null) {
					editPanel.initNewMap(mapManager.getMap());
				}
				fireEvent(MapEvent.ADDLAYER, null);
			}
			break;
		case CHANGELAYER:
			mapManager.setLayer(args[0]);

			if (mapManager.getLayer() instanceof TiledLayer) {
			} else if (mapManager.getLayer() instanceof PolygonLayer) {
			} else if (mapManager.getLayer() instanceof SpriteLayer) {
				if (((HybridMap) mapManager.getMap()).getCurrentSpriteTex() >= spriteSets
						.get(((SpriteLayer) ((HybridMap) mapManager.getMap())
								.getLayer(mapManager.getCurrentLayerIndex()))
								.getSpriteSetId()).getSprites().size())
					((HybridMap) mapManager.getMap()).setCurrentSpriteTex(0);
			}
			break;
		case ADDLAYER:
			if (mapManager.getMap() != null) {
				StringBuffer name = new StringBuffer();
				args = addLayerPrompt(name);
				if (args != null) {
					if (args[0] == 1) {
						((HybridMap) mapManager.getMap()).addPolyLayer(name
								.toString());
						polyPanel.refreshPolyPanel(
								(HybridMap) mapManager.getMap(), mapManager
										.getMap().getLayers().size() - 1);
					} else if (args[0] == 2) {

						if (args[1] >= 0 && args[1] < spriteSets.size())
							((HybridMap) mapManager.getMap()).addSpriteLayer(
									name.toString(), ((SpriteSet) spriteSets
											.values().toArray()[args[1]])
											.getId());
						else {
							fireEvent(MapEvent.ADDSPRITESET, args);
							((HybridMap) mapManager.getMap()).addSpriteLayer(
									name.toString(), ((SpriteSet) spriteSets
											.values().toArray()[spriteSets
											.values().size() - 1]).getId());
						}
					} else {
						if (tileSets.size() == 0)
							fireEvent(MapEvent.SETTILESET, null);
						((TiledMap) mapManager.getMap()).addOffsetLayer(name
								.toString(), ((TileSet) tileSets.values()
								.toArray()[args[3]]).getId(), args[1], args[2]);
					}

					mapManager
							.setLayer(mapManager.getMap().getLayers().size() - 1);
				}
			}
			break;
		case REMOVELAYER:
			if (args != null) {
				if (mapManager.getMap() != null) {
					mapManager.getMap().removeLayer(
							mapManager.getCurrentLayerIndex());
					mapManager
							.setLayer(mapManager.getMap().getLayers().size() - 1);
				}
			}
			break;
		case SAVEMAP:
			mapManager.saveMap();
			break;
		case OPENMAP:
			mapManager.openMap(tileSets, spriteSets);
			if (mapManager.getMap() != null) {
				for (Object o : mapManager.getMap().getLayers())
					if (o instanceof TiledLayer)
						if (tileSets.get(((TiledLayer) o).getTileSetId()) == null)
							fireEvent(MapEvent.SETTILESET, null);
						else if (o instanceof SpriteLayer)
							if (spriteSets.get(((SpriteLayer) o)
									.getSpriteSetId()) == null)
								fireEvent(MapEvent.ADDSPRITESET, null);
				editPanel.initNewMap(mapManager.getMap());
			}
			break;
		case EXPORTMAP:
			if (mapManager.getMap() != null) {
				args = exportPrompt();
				if (args != null) {
					IOType et = null;
					switch (args[0]) {
					case 1:
						et = IOType.PNG;
						break;
					case 2:
						et = IOType.TXT;
						break;
					case 3:
						et = IOType.XML;
						break;
					case 4:
						et = IOType.JSON;
						break;
					default:
						et = IOType.USER;
					}
					mapManager.exportMap(et, args[0] - 4, null);// tileSets.get(((TiledLayer)mapManager.getLayer()).getTileSetId()).getTileSet());
				}
			}
			break;
		case IMPORTMAP:
			if (args != null) {
				IOType it = null;
				switch (args[0]) {
				case 1:
					it = IOType.TXT;
					break;
				case 2:
					it = IOType.XML;
					break;
				}
				mapManager.importMap(it);
			}
			break;
		case RESIZEMAP:

			if (mapManager.getMap() != null) {

				args = resizePrompt();
				if (args != null)
					editPanel.resizeMap(mapManager.getMap());
				mapManager.resizeMap(args[0], args[1], args[2], args[3],
						args[4], args[5]);
			}
			break;
		case PACKRESIZEMAP:

			if (mapManager.getMap() != null) {
				mapManager.optimalResize();
			}
			break;

		// VIEW
		case UPDATEINFO:
			int tilex = (int) (args[0] / (Settings.CurrentTileWidth * Settings.CurrentScale));
			int tiley = (int) (args[1] / (Settings.CurrentTileHeight * Settings.CurrentScale));
			editPanel.UpdateInfo("Tile: ( x:" + tilex + " y:" + tiley
					+ " )  Pos: ( x:" + args[0] + " y:" + args[1] + " )");
			break;
		case UPDATEVIEW:
			if (mapManager.getMap() != null) {
				editPanel.updatePanel(mapManager.getCurrentLayerIndex(),
						mapManager.getCurrentPolyIndex(),
						mapManager.getCurrentSpriteIndex());
				mapControlPanel.removeAll();
				if (mapManager.getLayer() instanceof TiledLayer) {
					mapControlPanel.add(tilePanel);
					tilePanel.refreshTilePanel(
							(TiledMap) mapManager.getMap(),
							tileSets.get(
									((TiledLayer) mapManager.getLayer())
											.getTileSetId()).getTileSet(),
							((TiledMap) mapManager.getMap()).getCurrentTex(),
							editorView.getWidth(), mapManager
									.getCurrentLayerIndex());
				} else if (mapManager.getLayer() instanceof PolygonLayer) {
					mapControlPanel.add(polyPanel);
					polyPanel.refreshPolyPanel((HybridMap) mapManager.getMap(),
							mapManager.getCurrentLayerIndex());
				} else if (mapManager.getLayer() instanceof SpriteLayer) {
					mapControlPanel.add(spritePanel);
					spritePanel.refreshSpritePanel(
							((HybridMap) mapManager.getMap())
									.getCurrentSpriteTex(),
							editorView.getWidth(),
							mapManager.getCurrentLayerIndex(),
							spriteSets.get(
									((SpriteLayer) mapManager.getLayer())
											.getSpriteSetId()).getSprites(),
							((SpriteLayer) mapManager.getLayer()).getSprites());
				}
			}
			editorView.validate();
			break;
		case SETSCALE:
			Settings.CurrentScale = args[0] / 100.0f;
			me = MapEvent.UPDATEVIEW;
			break;
		case SETPOLYTHICKNESS:
			Settings.PolygonLineThickness = args[0];
			break;
		case SETBACKXOR:
			editPanel.getMapCanvas().setBackXor(
					!editPanel.getMapCanvas().isBackXor());
			break;
		case BACKGROUNDCOLOR:
			editPanel.getMapCanvas().setBackgroundColor(
					JColorChooser.showDialog(null, "Velg farge", editPanel
							.getMapCanvas().getBackgroundColor()));
			break;
		case POLYGONCOLOR:
			editPanel.getMapCanvas().setPolygonColor(
					JColorChooser.showDialog(null, "Velg farge", editPanel
							.getMapCanvas().getPolygonColor()));
			break;
		case SPRITECOLOR:
			editPanel.getMapCanvas().setSpriteColor(
					JColorChooser.showDialog(null, "Velg farge", editPanel
							.getMapCanvas().getSpriteColor()));
			break;
		case ONIONSKIN:
			editPanel.getMapCanvas().setOnionskin(
					!editPanel.getMapCanvas().isOnionskin());
			break;
		case GRID:
			editPanel.getMapCanvas()
					.setGrid(!editPanel.getMapCanvas().isGrid());
			break;
		case BACKDARKENED:
			editPanel.getMapCanvas().setBackDarkened(
					!editPanel.getMapCanvas().isBackDarkened());
			break;
		case CHANGEXOFFSET:
			editPanel.getMapCanvas().setScreenOffsetX(args[0]);
			break;
		case CHANGEYOFFSET:
			editPanel.getMapCanvas().setScreenOffsetY(args[0]);
			break;

		// TILE
		case SETTILE:
			if (args != null && (mapManager.getLayer() instanceof TiledLayer)
					&& !(args[2] < 0))
				if (toolPanel.currentBrush != null)
					fireEvent(MapEvent.SETBRUSH, args);
				else {
					if (toolPanel.floodFill.isSelected())
						((TiledMap) mapManager.getMap())
								.floodFill(
										(int) (args[0] / (Settings.CurrentTileWidth * Settings.CurrentScale)),
										(int) (args[1] / (Settings.CurrentTileHeight * Settings.CurrentScale)),
										args[2]);
					else
						((TiledMap) mapManager.getMap())
								.setTile(
										(int) (args[0] / (Settings.CurrentTileWidth * Settings.CurrentScale)),
										(int) (args[1] / (Settings.CurrentTileHeight * Settings.CurrentScale)),
										args[2], false);
				}
			break;
		case UNDO:
			if (mapManager.getMap() != null) {
				int untex = ((TiledMap) mapManager.getMap()).getCurrentTex();
				int[] undo = ((TiledMap) mapManager.getMap())
						.getChangeHandler().undo();
				if (undo != null) {
					((TiledMap) mapManager.getMap()).setCurrentTex(undo[2]);
					((TiledMap) mapManager.getMap()).setTile(undo[0], undo[1],
							undo[3], true);
					((TiledMap) mapManager.getMap()).setCurrentTex(untex);
				}
			}
			break;
		case REDO:
			if (mapManager.getMap() != null) {
				int retex = ((TiledMap) mapManager.getMap()).getCurrentTex();
				int[] redo = ((TiledMap) mapManager.getMap())
						.getChangeHandler().redo();
				if (redo != null) {
					((TiledMap) mapManager.getMap()).setCurrentTex(redo[2]);
					((TiledMap) mapManager.getMap()).setTile(redo[0], redo[1],
							redo[3], false);
					((TiledMap) mapManager.getMap()).setCurrentTex(retex);
				}
			}
			break;
		case ADDBRUSH:
			if (mapManager.getLayer() instanceof TiledLayer)
				toolPanel.addBrush(new Rectangle(args[0], args[1], args[2],
						args[3]), (TiledLayer) mapManager.getLayer());
			break;
		case SETBRUSH:
			if (args != null)
				for (int y = 0; y < toolPanel.currentBrush.length; y++) {
					for (int x = 0; x < toolPanel.currentBrush[y].length; x++) {
						((TiledMap) mapManager.getMap())
								.setCurrentTex(toolPanel.currentBrush[y][x]);

						((TiledMap) mapManager.getMap())
								.setTile(
										(int) (args[0] / (Settings.CurrentTileWidth * Settings.CurrentScale))
												+ x,
										(int) (args[1] / (Settings.CurrentTileHeight * Settings.CurrentScale))
												+ y, args[2], false);
					}
				}
			break;
		case SELECTION:
			editPanel.getMapCanvas().setSelecting(true);
			break;
		case SETCURRENTTILE:
			if (args != null && mapManager.getMap() != null) {
				toolPanel.currentBrush = null;
				((TiledMap) mapManager.getMap()).setCurrentTex(args[0]);
			}
			break;
		case ADDTILESET:
			if (mapManager.getMap() != null) {
				Vector<File> files = new Vector<File>(0);
				TileSet ts = ImageLoader.loadTileset(JOptionPane
						.showInputDialog(null,
								"Please enter a name for this spriteset"),
						files);
				ts.setFiles(files);
				((TiledMap) mapManager.getMap()).getTileSets().put(ts.getId(),
						ts.getFiles());

				tileSets.put(ts.getId(), ts);
			}
			break;
		case SETTILESET:
			if (mapManager.getMap() != null) {
				if (args == null)
					args = manageTileSetPrompt();
				if (args[0] < 0) {
					fireEvent(MapEvent.ADDTILESET, null);
					args[0] = tileSets.size() - 1;
				}
				if (args[0] >= 0 && args[0] < tileSets.size()) {
					TileSet ts = (TileSet) tileSets.values().toArray()[args[0]];

					if (tileSets.size() > 0
							&& ts.getTileSet().size() >= tileSets
									.get(ts.getId()).getTileSet().size()) {

						((TiledMap) mapManager.getMap()).getTileSets().remove(
								ts.getId());
						tileSets.remove(ts.getId());
						tileSets.put(ts.getId(), ts);
					} else {
						((TiledMap) mapManager.getMap()).getTileSets().put(
								ts.getId(), ts.getFiles());
						tileSets.put(ts.getId(), ts);
					}

					editPanel.getMapCanvas().setTileSets(tileSets);
				}
			}
			break;

		// SPRITE
		case SETCURRENTSPRITE:
			if (args != null && mapManager.getMap() != null) {
				((HybridMap) mapManager.getMap()).setCurrentSpriteTex(args[0]);
			}
			break;
		case ADDSPRITE:
			if (mapManager.getLayer() instanceof SpriteLayer) {

				int x = (int) (args[0] / Settings.CurrentScale);
				int y = (int) (args[1] / Settings.CurrentScale);
				if (spritePanel.isSnapToGrid()) {

					if (x % Settings.CurrentTileWidth > Settings.CurrentTileWidth / 2)
						x = (x / Settings.CurrentTileWidth)
								* Settings.CurrentTileWidth
								+ Settings.CurrentTileWidth;
					else
						x = (x / Settings.CurrentTileWidth)
								* Settings.CurrentTileWidth;

					if (y % Settings.CurrentTileHeight > Settings.CurrentTileHeight / 2)
						y = (y / Settings.CurrentTileHeight)
								* Settings.CurrentTileHeight
								+ Settings.CurrentTileHeight;
					else
						y = (y / Settings.CurrentTileHeight)
								* Settings.CurrentTileHeight;
				}
				((SpriteLayer) mapManager.getLayer())
						.addSprite(
								new Point(x, y),
								((HybridMap) mapManager.getMap())
										.getCurrentSpriteTex(),
								spriteSets
										.get(((SpriteLayer) mapManager
												.getLayer()).getSpriteSetId())
										.getFiles()
										.get(((HybridMap) mapManager.getMap())
												.getCurrentSpriteTex())
										.getName());
			}
			break;
		case REMOVESPRITE:
			if (mapManager.getLayer() instanceof SpriteLayer) {
				if (args[0] == -1)
					((SpriteLayer) mapManager.getLayer())
							.removeSprite(((SpriteLayer) mapManager.getLayer())
									.getSprites().size() - 1);
				else
					((SpriteLayer) mapManager.getLayer()).removeSprite(args[0]);
			}
			break;
		case CHANGESPRITE:
			if (mapManager.getLayer() instanceof SpriteLayer) {
				mapManager.setSprite(args[0]);
			}
			break;
		case SETSPRITESET:

			if (mapManager.getMap() != null) {
				if (args == null)
					args = manageSpriteSetPrompt();
				if (args[0] < 0) {
					fireEvent(MapEvent.ADDSPRITESET, null);
					args[0] = spriteSets.size() - 1;
				}

				if (args[0] >= 0 && args[0] < spriteSets.size()) {
					SpriteSet ss = (SpriteSet) spriteSets.values().toArray()[args[0]];
					for (SpriteLayer layer : ((HybridMap) mapManager.getMap())
							.getSpriteLayers())
						for (Sprite s : layer.getSprites())
							if (layer.getSpriteSetId() == spriteSets.get(
									layer.getSpriteSetId()).getId()
									&& s.getVal() >= ss.getSprites().size())
								return;

					((HybridMap) mapManager.getMap()).getSpriteSets().remove(
							ss.getId());
					((HybridMap) mapManager.getMap()).getSpriteSets().put(
							ss.getId(), ss.getFiles());

					spriteSets.remove(ss.getId());
					spriteSets.put(ss.getId(), ss);

					if (((HybridMap) mapManager.getMap()).getCurrentSpriteTex() >= ss
							.getSprites().size())
						((HybridMap) mapManager.getMap())
								.setCurrentSpriteTex(0);
				}
			}
			break;
		case ADDSPRITESET:
			if (mapManager.getMap() != null) {
				Vector<File> files = new Vector<File>(0);
				SpriteSet ss = ImageLoader.loadSpriteSet(JOptionPane
						.showInputDialog(null,
								"Please enter a name for this spriteset"),
						files);
				ss.setFiles(files);

				((HybridMap) mapManager.getMap()).getSpriteSets().put(
						ss.getId(), ss.getFiles());
				spriteSets.put(ss.getId(), ss);
			}
			break;
		//EDIT
		case EDITMODE:
			editPanel.setEditMode(true);
			editPanel.setSelectMode(false);
			break;
		case SELECTMODE:
			editPanel.setEditMode(false);
			editPanel.setSelectMode(true);
			break;	
		// POLYGON
		case ADDPOINT:
			if (mapManager.getLayer() instanceof PolygonLayer) {
				polyPanel.addPoint(new Point(
						(int) (args[0] / Settings.CurrentScale),
						(int) (args[1] / Settings.CurrentScale)));
			}
			break;
		case MOVEPOINT:
			if (mapManager.getLayer() instanceof PolygonLayer) {
				polyPanel.movePoint(args[0], new Point(
						(int) (args[1] / Settings.CurrentScale),
						(int) (args[2] / Settings.CurrentScale)));
			}
			break;
		case INFLATE:
			if (mapManager.getLayer() instanceof PolygonLayer) {
				args = inflatePrompt();
				if (args != null)
					polyPanel.inflate(args[0]);
			}
			break;
		case REMOVEPOINT:
			if (mapManager.getLayer() instanceof PolygonLayer) {
				if (args[0] == -1)
					polyPanel
							.removePoint(polyPanel.getCurrentPoints().size() - 1);
				else
					polyPanel.removePoint(args[0]);
			}
			break;
		case ADDPOLYGON:
			if (mapManager.getLayer() instanceof PolygonLayer) {
				((PolygonLayer) mapManager.getLayer()).addPolygon(polyPanel
						.getCurrentPoints());
				mapManager.setPoly(((PolygonLayer) mapManager.getLayer())
						.size() - 1);
			}
			break;
		case REMOVEPOLYGON:
			if (args[0] >= 0 && mapManager.getLayer() instanceof PolygonLayer) {
				((PolygonLayer) mapManager.getLayer()).removePolygon(args[0]);
			}
			break;
		case CHANGEPOLYGON:
			if (mapManager.getLayer() instanceof PolygonLayer) {
				if (args[0] < ((PolygonLayer) mapManager.getLayer()).size()) {
					polyPanel.setCurrentPoints(((PolygonLayer) mapManager
							.getLayer()).getPolygon(args[0]));
					mapManager.setPoly(args[0]);
				}
			}
			break;
		}
		if (!me.equals(MapEvent.UPDATEVIEW) && !me.equals(MapEvent.UPDATEINFO)
				&& !me.equals(MapEvent.CHANGEXOFFSET)
				&& !me.equals(MapEvent.CHANGEYOFFSET)) {
			fireEvent(MapEvent.UPDATEVIEW, null);
		} else
			editorView.repaint();

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {
		int[] args = new int[1];
		switch (e.getKeyChar()) {
		case KeyEvent.VK_1:
			args[0] = 1;
			break;
		case KeyEvent.VK_2:
			args[0] = 2;
			break;
		case KeyEvent.VK_3:
			args[0] = 3;
			break;
		case KeyEvent.VK_4:
			args[0] = 4;
			break;
		case KeyEvent.VK_5:
			args[0] = 5;
			break;
		case KeyEvent.VK_6:
			args[0] = 6;
			break;
		case KeyEvent.VK_7:
			args[0] = 7;
			break;
		case KeyEvent.VK_8:
			args[0] = 8;
			break;
		case KeyEvent.VK_9:
			args[0] = 9;
			break;
		}
		if (mapManager.getLayer() instanceof TiledLayer)
			fireEvent(MapEvent.SETCURRENTTILE, args);
		else if (mapManager.getLayer() instanceof PolygonLayer)
			fireEvent(MapEvent.CHANGEPOLYGON, args);
	}

	/**
	 * Creates a new prompt asking the user for input when trying to import a
	 * map.
	 * 
	 * @return int[]
	 */
	private int[] inflatePrompt() {
		JPanel inflatePanel = new JPanel(new GridLayout(2, 2));
		JLabel lp = new JLabel("Amount: ");
		JTextField amount = new JTextField();

		inflatePanel.add(lp);
		inflatePanel.add(amount);

		int[] args = null;
		if (JOptionPane.showConfirmDialog(null, inflatePanel,
				"Inflate Polygon", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
			try {
				args = new int[6];
				if (amount.getText().length() == 0)
					args[0] = -1;
				else
					args[0] = Integer.parseInt(amount.getText());
			} catch (Exception ex) {
				return null;
			}
		return args;
	}

	private int[] resizePrompt() {
		JPanel resizePanel = new JPanel(new GridLayout(7, 2));
		JLabel w = new JLabel("Width (Empty assumes current width): ");
		JLabel h = new JLabel("Height (Empty assumes current height): ");

		JLabel p = new JLabel("Padding");
		p.setToolTipText("Use these to add tiles in a particular direction. Will be added on top of the specified size.");
		JLabel lp = new JLabel("Left Padding: ");
		JLabel rp = new JLabel("Right Padding: ");
		JLabel tp = new JLabel("Top Padding: ");
		JLabel bp = new JLabel("Bottom Padding: ");

		JTextField width = new JTextField();
		JTextField height = new JTextField();
		JTextField left = new JTextField();
		JTextField right = new JTextField();
		JTextField top = new JTextField();
		JTextField bottom = new JTextField();

		resizePanel.add(w);
		resizePanel.add(width);
		resizePanel.add(h);
		resizePanel.add(height);
		resizePanel.add(p);
		resizePanel.add(new JLabel());
		resizePanel.add(lp);
		resizePanel.add(left);
		resizePanel.add(rp);
		resizePanel.add(right);
		resizePanel.add(tp);
		resizePanel.add(top);
		resizePanel.add(bp);
		resizePanel.add(bottom);

		int[] args = null;
		if (JOptionPane.showConfirmDialog(null, resizePanel, "Resize map",
				JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
			try {
				args = new int[6];
				if (width.getText().length() == 0)
					args[0] = -1;
				else
					args[0] = Integer.parseInt(width.getText());
				if (width.getText().length() == 0)
					args[1] = -1;
				else
					args[1] = Integer.parseInt(height.getText());
				if (left.getText().length() == 0)
					args[2] = 0;
				else
					args[2] = Integer.parseInt(left.getText());
				if (right.getText().length() == 0)
					args[3] = 0;
				else
					args[3] = Integer.parseInt(right.getText());
				if (top.getText().length() == 0)
					args[4] = 0;
				else
					args[4] = Integer.parseInt(top.getText());
				if (bottom.getText().length() == 0)
					args[5] = 0;
				else
					args[5] = Integer.parseInt(bottom.getText());

			} catch (Exception ex) {
				return null;
			}
		return args;
	}

	/**
	 * Creates a new prompt asking the user for input when creating a new map.
	 * 
	 * @return int[5] containing MapWidth, MapHeight, TileWidth and TileHeight,
	 *         and 1 if Hybrid is selected or 0 if not. .
	 */
	private int[] newMapPrompt() {
		JPanel panel = new JPanel(new GridLayout(4, 2));

		JLabel mwLabel = new JLabel("Mapwidth");
		JLabel mhLabel = new JLabel("Mapheight");
		JLabel twLabel = new JLabel("Tilewidth");
		JLabel thLabel = new JLabel("Tileheight");
		JTextField mwField = new JTextField("10");
		JTextField mhField = new JTextField("10");
		JTextField twField = new JTextField("64");
		JTextField thField = new JTextField("64");
		panel.add(mwLabel);
		panel.add(mwField);
		panel.add(mhLabel);
		panel.add(mhField);
		panel.add(twLabel);
		panel.add(twField);
		panel.add(thLabel);
		panel.add(thField);

		int ret = JOptionPane.showConfirmDialog(null, panel, "New Map",
				JOptionPane.OK_CANCEL_OPTION);
		if (ret == JOptionPane.OK_OPTION) {
			// Read component values.
			try {
				int[] args = new int[4];
				boolean error = false;
				args[0] = Integer.parseInt(mwField.getText());
				if (args[0] > Settings.MAXWIDTH) {
					JOptionPane.showMessageDialog(null,
							"Mapwidth must be less than " + Settings.MAXWIDTH);
					error = true;
				} else if (args[0] < 0) {
					JOptionPane.showMessageDialog(null,
							"Mapwidth must be larger than " + 0);
					error = true;
				}
				args[1] = Integer.parseInt(mhField.getText());
				if (args[1] > Settings.MAXHEIGHT) {
					JOptionPane
							.showMessageDialog(null,
									"Maxheight must be less than "
											+ Settings.MAXHEIGHT);
					error = true;
				} else if (args[1] < 0) {
					JOptionPane.showMessageDialog(null,
							"Maxheight must be larger than " + 0);
					error = true;
				}
				args[2] = Integer.parseInt(twField.getText());
				if (args[2] > Settings.MAXTILEWIDTH) {
					JOptionPane.showMessageDialog(null,
							"Tilewidth must be less than "
									+ Settings.MAXTILEWIDTH);
					error = true;
				} else if (args[2] < Settings.MINTILEWIDTH) {
					JOptionPane.showMessageDialog(null,
							"Tilewidth must be larger than "
									+ Settings.MINTILEWIDTH);
					error = true;
				}
				args[3] = Integer.parseInt(thField.getText());
				if (args[3] > Settings.MAXTILEHEIGHT) {
					JOptionPane.showMessageDialog(null,
							"Tileheight must be less than "
									+ Settings.MAXTILEHEIGHT);
					error = true;
				} else if (args[3] < 0) {
					JOptionPane.showMessageDialog(null,
							"Tileheight must be larger than "
									+ Settings.MINTILEHEIGHT);
					error = true;
				}
				if (error)
					return null;
				return args;
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return null;
			}
		} else
			return null;
	}

	private int[] addTileLayerPrompt() {

		int tilesetIndex = manageTileSetPrompt()[0];
		if (tilesetIndex == -1) {
			fireEvent(MapEvent.ADDTILESET, null);
			tilesetIndex = 0;
		}
		JPanel panel = new JPanel(new GridLayout(5, 1));

		JLabel title = new JLabel(
				"Choose a tileset interval. The interval must be within 0 and "
						+ (((TileSet) tileSets.values().toArray()[tilesetIndex])
								.getTileSet().size() - 1));

		JLabel sLabel = new JLabel("First tile:");
		JLabel eLabel = new JLabel("Last tile:");
		JTextField sField = new JTextField("0");
		JTextField eField = new JTextField(String.valueOf(((TileSet) tileSets
				.values().toArray()[tilesetIndex]).getTileSet().size() - 1));

		panel.add(title);
		panel.add(sLabel);
		panel.add(sField);
		panel.add(eLabel);
		panel.add(eField);

		int ret = JOptionPane.showConfirmDialog(null, panel, "Choose tiles",
				JOptionPane.OK_CANCEL_OPTION);
		if (ret == JOptionPane.OK_OPTION) {
			int[] args = new int[4];
			try {
				boolean error = false;
				args[1] = Integer.parseInt(sField.getText());
				if (args[1] < 0) {
					JOptionPane.showMessageDialog(null,
							"First tile must be larger than " + 0);
					error = true;
				}
				args[2] = Integer.parseInt(eField.getText());
				if (args[2] > ((TileSet) tileSets.values().toArray()[tilesetIndex])
						.getTileSet().size() - 1) {
					JOptionPane
							.showMessageDialog(null,
									"Last tile in interval must be less than "
											+ (((TileSet) tileSets.values()
													.toArray()[tilesetIndex])
													.getTileSet().size() - 1));
					error = true;
				}

				if (args[2] - args[1] < 1) {
					JOptionPane.showMessageDialog(null,
							"The interval must be atleast 1");
					error = true;
				}
				args[0] = 0;
				args[3] = tilesetIndex;
				if (error)
					return null;
				return args;
			} catch (NumberFormatException e) {
				args[0] = 0;
				args[1] = 0;
				args[2] = ((TileSet) tileSets.values().toArray()[tilesetIndex])
						.getTileSet().size();
				args[3] = tilesetIndex;
				return args;
			}
		} else
			return null;
	}

	private int[] manageTileSetPrompt() {

		JPanel panel = new JPanel();

		JLabel title = new JLabel("Select tileset:");

		panel.add(title);

		JRadioButton newtileset = new JRadioButton("New tileset");
		newtileset
				.setToolTipText("Add a new set of tiles to use for this layer.");

		ButtonGroup group = new ButtonGroup();

		group.add(newtileset);
		panel.add(newtileset);

		Vector<JRadioButton> buttons = new Vector<JRadioButton>();
		for (TileSet t : tileSets.values()) {
			JRadioButton tileset = new JRadioButton(t.getId());
			group.add(tileset);
			panel.add(tileset);
			buttons.add(tileset);
		}
		panel.setLayout(new GridLayout(2 + buttons.size(), 1));

		int ret = JOptionPane.showConfirmDialog(null, panel, "",
				JOptionPane.OK_CANCEL_OPTION);
		if (ret == JOptionPane.OK_OPTION) {
			int[] args = new int[1];

			for (JRadioButton b : buttons)
				if (b.isSelected())
					args[0] = buttons.indexOf(b);

			if (newtileset.isSelected())
				args[0] = -1;

			return args;
		} else
			return null;
	}

	private int[] manageSpriteSetPrompt() {

		JPanel panel = new JPanel();

		JLabel title = new JLabel("Select spriteset:");

		panel.add(title);

		JRadioButton newspriteset = new JRadioButton("New spriteset");
		newspriteset
				.setToolTipText("Add a new set of sprites to use for this layer.");

		ButtonGroup group = new ButtonGroup();

		group.add(newspriteset);
		panel.add(newspriteset);

		Vector<JRadioButton> buttons = new Vector<JRadioButton>();
		for (SpriteSet s : spriteSets.values()) {
			JRadioButton spriteset = new JRadioButton(s.getId());
			group.add(spriteset);
			panel.add(spriteset);
			buttons.add(spriteset);
		}
		panel.setLayout(new GridLayout(2 + buttons.size(), 1));

		int ret = JOptionPane.showConfirmDialog(null, panel, "",
				JOptionPane.OK_CANCEL_OPTION);
		if (ret == JOptionPane.OK_OPTION) {
			int[] args = new int[1];

			for (JRadioButton b : buttons)
				if (b.isSelected())
					args[0] = buttons.indexOf(b);

			if (newspriteset.isSelected())
				args[0] = -1;

			return args;
		} else
			return null;
	}

	/**
	 * Creates a new prompt asking the user for input when adding a new layer.
	 * 
	 * @return int[3] containing FirstTileOffset, LastTileOffset and 1 if
	 *         Polygon is selected or 0 if not.
	 */
	private int[] addLayerPrompt(StringBuffer name) {
		JPanel panel = new JPanel(new GridLayout(3, 1));

		JTextField namefield = new JTextField("LayerName");

		JRadioButton tile = new JRadioButton("Tiled?");
		tile.setToolTipText("Good old tilebased layer.");
		JRadioButton poly = new JRadioButton("Polygon?");
		poly.setToolTipText("Polygonlayers are useful for specifying points in freeform.");
		JRadioButton sprite = new JRadioButton("Sprite?");
		sprite.setToolTipText("Spritelayers are useful for placing sprites around the map.");

		ButtonGroup group = new ButtonGroup();

		group.add(tile);
		group.add(poly);
		group.add(sprite);

		panel.add(namefield);
		panel.add(tile);
		panel.add(poly);
		panel.add(sprite);

		int ret = JOptionPane.showConfirmDialog(null, panel,
				"Choose layer type", JOptionPane.OK_CANCEL_OPTION);
		name.append(namefield.getText());
		if (tile.isSelected())
			return addTileLayerPrompt();
		else {
			if (ret == JOptionPane.OK_OPTION) {
				int[] args = new int[1];

				if (sprite.isSelected()) {
					args = new int[2];
					args[0] = 2;
					args[1] = manageSpriteSetPrompt()[0];
				} else if (poly.isSelected())
					args[0] = 1;

				return args;
			} else
				return null;
		}
	}

	/**
	 * Creates a new prompt asking the user for input when trying to import a
	 * map.
	 * 
	 * @return int[]
	 */
	private int[] importPrompt() {
		JPanel panel = new JPanel(new GridLayout(3, 1));

		JLabel title = new JLabel("Import:");
		JRadioButton XML = new JRadioButton("XML");
		JRadioButton TXT = new JRadioButton("TXT");
		XML.setSelected(true);
		panel.add(title);
		panel.add(XML);
		panel.add(TXT);

		ButtonGroup group = new ButtonGroup();
		group.add(XML);
		group.add(TXT);

		int ret = JOptionPane.showConfirmDialog(null, panel, "Import",
				JOptionPane.OK_CANCEL_OPTION);
		if (ret == JOptionPane.OK_OPTION) {
			int[] args = new int[1];
			if (XML.isSelected()) {
				args[0] = 1;
			} else if (TXT.isSelected()) {
				args[0] = 2;
			}
			return args;
		} else
			return null;
	}

	/**
	 * Creates a new prompt asking the user for input when exporting a map.
	 * 
	 * @return int[1] containing 1 if PNG, 2 if TXT, 3 if XML or the index of
	 *         the user script otherwise.
	 */
	private int[] exportPrompt() {
		File[] userScripts = ScriptInterpreter.getAvailableExportScripts();

		Vector<JRadioButton> userButtons = new Vector<JRadioButton>(
				userScripts.length);

		JPanel panel = new JPanel(new GridLayout(5 + userScripts.length, 1));

		JLabel title = new JLabel("Export as:");
		JRadioButton PNG = new JRadioButton("PNG");
		JRadioButton XML = new JRadioButton("XML");
		JRadioButton TXT = new JRadioButton("TXT");
		JRadioButton JSON = new JRadioButton("JSON");

		PNG.setSelected(true);
		panel.add(title);
		panel.add(PNG);
		panel.add(XML);
		panel.add(TXT);
		panel.add(JSON);

		ButtonGroup group = new ButtonGroup();
		group.add(PNG);
		group.add(TXT);
		group.add(XML);
		group.add(JSON);

		for (int i = 0; i < userScripts.length; i++) {
			JRadioButton scr = new JRadioButton(userScripts[i].getName());
			panel.add(scr);
			group.add(scr);
			userButtons.add(scr);
		}

		int ret = JOptionPane.showConfirmDialog(null, panel, "Export",
				JOptionPane.OK_CANCEL_OPTION);
		if (ret == JOptionPane.OK_OPTION) {
			int[] args = new int[1];
			if (PNG.isSelected()) {
				args[0] = 1;
			} else if (TXT.isSelected()) {
				args[0] = 2;
			} else if (XML.isSelected()) {
				args[0] = 3;
			} else if (JSON.isSelected()) {
				args[0] = 4;
			} else
				for (JRadioButton b : userButtons)
					if (b.isSelected())
						args[0] = 5 + userButtons.indexOf(b);

			return args;
		} else
			return null;
	}

	/**
	 * Program main method.
	 */
	public static void main(String[] args) {

		new ViewController();
	}
}