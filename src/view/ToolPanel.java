package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import logic.TiledLayer;
import view.ViewController.MapEvent;

/**
 * The ToolPanel provides for the creation and organization of brushes.
 */
public class ToolPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -8356683981327236164L;
	ViewController viewController;
	JToggleButton floodFill;
	JButton cbrush;
	JButton defaultBrush;
	JButton removeBrush;
	Vector<JButton> brushBtns;
	private Vector<int[][]> brushes;
	int[][] currentBrush;
	boolean brushSelected;
	boolean removeSelected;

	JPanel jpbtns;

	/**
	 * Constructor.
	 * 
	 * @param ViewController
	 *            - The {@Link ViewController} to be used when firing
	 *            events.
	 * @param frameWidth
	 *            - The width of the editor.
	 * @param framHeight
	 *            - The height of the editor.
	 */
	public ToolPanel(ViewController view, int frameWidth, int frameHeight) {
		viewController = view;
		jpbtns = new JPanel();
		add(jpbtns);

		setBorder(Settings.COMPBORDER);
		initPanel();

		setPreferredSize(new Dimension(frameWidth / 4, frameHeight));
	}

	/**
	 * Initializes the toolpanel.
	 */
	public void initPanel() {
		jpbtns.removeAll();
		GridLayout grid = new GridLayout(4, 1, 0, 2);
		jpbtns.setLayout(grid);

		JLabel jl = new JLabel("<html><u>Toolbox</u></html>");
		// Knapper
		floodFill = new JToggleButton("Enable Floodfill");
		floodFill.addActionListener(this);
		floodFill
				.setToolTipText("Enable if you want tiles to flood fill, replacing all tiles that are the same as the one clicked.");
		floodFill.setBackground(Color.LIGHT_GRAY);

		cbrush = new JButton("Create brush");
		cbrush.addActionListener(this);
		cbrush.setToolTipText("Select the tiles to be the new brush.");
		cbrush.setBackground(Color.LIGHT_GRAY);

		removeBrush = new JButton("Remove brush");
		removeBrush.addActionListener(this);
		removeBrush.setToolTipText("Remove a brush by pressing its button.");
		removeBrush.setBackground(Color.LIGHT_GRAY);

		brushBtns = new Vector<JButton>();

		jpbtns.add(jl);
		jpbtns.add(floodFill);
		jpbtns.add(cbrush);
		jpbtns.add(removeBrush);

		brushes = new Vector<int[][]>();
	}

	/**
	 * Use the addBrush method to create a new int[][] array and add it to the
	 * list of available brushes. Will also repopulate the panel components.
	 * 
	 * @param brush
	 *            - Rectangle that provides the constrains of the maparea that
	 *            is used when extracting tiles to use in the new brush.
	 * @param layers
	 *            - the arrays of the layers that are used when getting texture
	 *            values for the brushtiles.
	 */
	public void addBrush(Rectangle brush, TiledLayer layer) {

		int minX = (int) (brush.getMinX() / (Settings.CurrentTileWidth * Settings.CurrentScale));
		int minY = (int) (brush.getMinY() / (Settings.CurrentTileHeight * Settings.CurrentScale));
		int maxX = (int) (brush.getMaxX() / (Settings.CurrentTileWidth * Settings.CurrentScale));
		int maxY = (int) (brush.getMaxY() / (Settings.CurrentTileHeight * Settings.CurrentScale));

		if (minY < 0)
			minY = 0;
		if (maxY > layer.getLayer().length)
			maxY = layer.getLayer().length;
		if (minX < 0)
			minX = 0;
		if (maxX > layer.getLayer()[0].length)
			maxX = layer.getLayer()[0].length;

		int[][] brushArray = new int[maxY - minY][maxX - minX];
		for (int y = minY; y < maxY; y++) {
			for (int x = minX; x < maxX; x++) {
				int tex = layer.getLayer()[y][x];
				brushArray[y - minY][x - minX] = tex;
			}
		}
		brushes.add(brushArray);
		currentBrush = brushArray;

		JButton newBrush = new JButton(
				JOptionPane.showInputDialog("Please enter a brush name."));
		newBrush.setName(Integer.toString(brushBtns.size()));
		newBrush.addActionListener(this);
		newBrush.setBackground(Color.LIGHT_GRAY);
		brushBtns.add(newBrush);
		reBuildPanel();
	}

	/**
	 * Removes all the components from the toolpanel and sets it up again.
	 */
	private void reBuildPanel() {
		jpbtns.removeAll();
		GridLayout grid = new GridLayout(brushBtns.size() + 4, 1, 0, 2);
		jpbtns.setLayout(grid);

		JLabel jl = new JLabel("<html><u>Toolbox</u></html>");
		jpbtns.add(jl);
		jpbtns.add(floodFill);
		jpbtns.add(cbrush);
		jpbtns.add(removeBrush);

		for (JButton b : brushBtns) {
			jpbtns.add(b);
		}

		validate();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == floodFill) {
			if (floodFill.isSelected()) {
				int rVal = JOptionPane
						.showConfirmDialog(
								null,
								"Floodfill operations can not be reversed, are you sure you want to enter floodfill mode? Make sure you saved the map recently!",
								"Enable floodfill mode?",
								JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE);

				if (rVal == JOptionPane.NO_OPTION) {
					floodFill.setSelected(false);
				}
			}
		} else if (e.getSource() == cbrush) {
			if (brushBtns.size() > 20)
				JOptionPane.showMessageDialog(null, "Too many brushes");
			else {
				viewController.fireEvent(MapEvent.SELECTION, null);
			}
		} else {
			if (e.getSource() == removeBrush) {
				removeSelected = true;
			}
			for (JButton b : brushBtns) {
				if (e.getSource() == b) {
					if (removeSelected && b != cbrush && b != removeBrush
							&& b != defaultBrush) {
						brushBtns.remove(b);
						brushes.remove(Integer.parseInt(b.getName()));
						removeSelected = false;
						currentBrush = null;
						reBuildPanel();
						break;
					}
					currentBrush = brushes.get(Integer.parseInt(b.getName()));
					b.setBackground(Color.WHITE);
				} else
					b.setBackground(Color.LIGHT_GRAY);
			}
		}
	}
}
