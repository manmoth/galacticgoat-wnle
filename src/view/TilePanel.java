package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import view.ViewController.MapEvent;

import logic.TiledMap;

/**
 * The TilePanel class provides a JScrollPane that is used to give an easy
 * interface with the tileset when creating a map. The panel will be populated
 * with buttons that have the tile-image as icon so that a button press is used
 * to select the image to be used.
 */
public class TilePanel extends JScrollPane implements ActionListener {
	private static final long serialVersionUID = -1990086263605998147L;
	private ViewController viewController;
	private JPanel tilePanel;
	private Vector<JButton> tileBtns;

	/**
	 * Constructor.
	 * 
	 * @param ViewController
	 *            - The {@Link ViewController} to be used when firing
	 *            events.
	 * @param frameWidth
	 *            - The width of the editor.
	 * @param framHeight
	 *            - The height of the editor.
	 * 
	 * */
	public TilePanel(ViewController viewController, int frameWidth,
			int frameHeight) {
		this.viewController = viewController;
		tilePanel = new JPanel();
		setBorder(Settings.COMPBORDER);
		setPreferredSize(new Dimension(frameWidth / 3, frameHeight));
		getViewport().add(tilePanel);
	}

	/**
	 * This method is used whenever a new tileset is imported. It lays out two
	 * columns of buttons with the tileset images as icons.
	 * 
	 * @param TiledMap
	 *            - The TiledMap provides the width and height of the tiles.
	 *            that is used when extracting tiles to use in the new brush.
	 * @param layers
	 *            - The arrays of the layers that are used when getting texture
	 *            values for the brushtiles.
	 * @param frameWidth
	 *            - The width of the frame that contains the tilePanel. This is
	 *            used to control the width of the panel so that it does not
	 *            take up too much space.
	 */
	public void refreshTilePanel(TiledMap tiledMap,
			Vector<BufferedImage> tileSet, int currentTile, int frameWidth,
			int layerNum) {
		if (tileSet != null && tiledMap.getTileLayers().size() > 0) {
			tilePanel.removeAll();

			int layerTileOffset = (tiledMap).getLayerTileOffsets(layerNum)[0];
			int layerTiles = (tiledMap).getLayerTileOffsets(layerNum)[1];

			getVerticalScrollBar().setUnitIncrement(Settings.CurrentTileHeight);
			getHorizontalScrollBar()
					.setUnitIncrement(Settings.CurrentTileWidth);

			tileBtns = new Vector<JButton>();
			for (int i = 0; i < tileSet.size(); i++) {
				if (i == 0 || (i >= layerTileOffset && i <= layerTiles)
						|| layerTiles == -1) {
					JButton tbtn = new JButton(new ImageIcon(
							tileSet.elementAt(i)));
					tbtn.setToolTipText("Use the number keys to quickly select a tile.");
					if (i == currentTile)
						tbtn.setBackground(Color.BLACK);
					tbtn.setPreferredSize(new Dimension(
							Settings.CurrentTileWidth,
							Settings.CurrentTileHeight));
					int width = (Settings.CurrentTileWidth * 2) + 20;
					if (width < frameWidth / 3)
						setPreferredSize(new Dimension(width, getHeight()));
					else
						setPreferredSize(new Dimension(frameWidth / 3,
								getHeight()));
					tbtn.setName(Integer.toString(i));
					tbtn.addActionListener(this);
					tileBtns.add(tbtn);
				}
			}

			GridLayout gl;

			gl = new GridLayout((int) Math.ceil((tileBtns.size() + 1) / 2.0), 2);

			tilePanel.setLayout(gl);

			for (JButton b : tileBtns)
				tilePanel.add(b);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int[] args = new int[1];
		for (JButton b : tileBtns)
			if (e.getSource() == b)
				args[0] = Integer.parseInt(b.getName());

		viewController.fireEvent(MapEvent.SETCURRENTTILE, args);
	}

}
