package view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import view.ViewController.MapEvent;
import data.ScriptInterpreter;

/**
 * The EditorView class is used to set up the JFrame of the editor itself, the
 * menubar and all events from these objects. It takes most of the input from
 * the user and calls on the ViewController to fire events when changes are to
 * be made.
 */
public class EditorView extends JFrame implements WindowListener,
		ChangeListener, ActionListener {
	private static final long serialVersionUID = -5103224077435430287L;

	private ViewController viewController;

	private JMenu jmFile;
	private JMenu jmEdit;
	private JMenu jmLayers;
	private JMenu jmMap;
	private JMenu jmTiles;
	private JMenu jmHelp;

	// File buttons
	private JMenuItem jmFileNew;
	private JMenuItem jmFileOpen;
	private JMenuItem jmFileSave;
	private JMenuItem jmFileImp;
	private JMenuItem jmFileExp;
	private JMenuItem jmFileAddScr;
	private JMenuItem jmFileExit;

	// Edit buttons
	private JMenuItem jmEditRedo;
	private JMenuItem jmEditUndo;
	private JMenuItem jmEditResize;
	private JMenuItem jmEditPackResize;

	// Layer buttons
	private JMenuItem jmLayAdd;
	private JMenuItem jmLayRename;
	private JMenuItem jmLayRemove;

	// Map buttons
	private JMenuItem jmMapColor;
	private JCheckBoxMenuItem jmMapBackXor;
	private JMenuItem jmMapSpriteColor;
	private JMenuItem jmMapPolyColor;
	private JLabel jmMapScaleLabel;
	private JSlider jmMapScaleSlider;
	private JLabel jmMapPolyLabel;
	private JSlider jmMapPolySlider;
	private JCheckBoxMenuItem jmMapGrid;
	private JCheckBoxMenuItem jmMapOnion;
	private JCheckBoxMenuItem jmMapBLD;

	// Tiles buttons
	private JMenuItem jmTilesChange;
	private JMenuItem jmSpritesChange;

	// Help buttons
	private JMenuItem jmHelpInfo;

	/**
	 * Constructor. Will setup the menubar and all frame specific settings.
	 * 
	 * @param viewController
	 *            - A reference to the ViewController used to fire events.
	 */
	public EditorView(ViewController viewController) {
		this.viewController = viewController;
		// Setter opp vinduet, midtsetter bildet alltid
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int x = dim.width;
		int y = dim.height;
		setLocation(x / 2 - x / 4, y / 2 - y / 3);
		setSize(x / 2, y - (y / 3));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setTitle("Map Editor");

		initMenuBar();

		addWindowListener(this);
		addKeyListener(viewController);
		setVisible(true);
	}

	/**
	 * Sets up the menubar.
	 */
	private void initMenuBar() {
		// Menyer
		JMenuBar jmb = new JMenuBar();
		setJMenuBar(jmb);
		jmFile = new JMenu("File");
		jmEdit = new JMenu("Edit");
		jmLayers = new JMenu("Layers");
		jmMap = new JMenu("Map");
		jmTiles = new JMenu("Tiles");
		jmHelp = new JMenu("Help");

		jmb.add(jmFile);
		jmb.add(jmEdit);
		jmb.add(jmLayers);
		jmb.add(jmMap);
		jmb.add(jmTiles);
		jmb.add(jmHelp);

		// File buttons
		jmFileNew = new JMenuItem("New");
		jmFileNew.addActionListener(this);
		jmFileOpen = new JMenuItem("Open");
		jmFileOpen.addActionListener(this);
		jmFileSave = new JMenuItem("Save");
		jmFileSave.addActionListener(this);
		jmFileImp = new JMenuItem("Import");
		jmFileImp.addActionListener(this);
		jmFileExp = new JMenuItem("Export");
		jmFileExp.addActionListener(this);
		jmFileAddScr = new JMenuItem("Add Import/Export script");
		jmFileAddScr.addActionListener(this);
		jmFileExit = new JMenuItem("Exit");
		jmFileExit.addActionListener(this);
		jmFile.add(jmFileNew);
		jmFile.add(jmFileOpen);
		jmFile.add(jmFileSave);
		jmFile.add(jmFileImp);
		jmFile.add(jmFileExp);
		jmFile.add(jmFileAddScr);
		jmFile.add(jmFileExit);

		// Edit buttons
		jmEditUndo = new JMenuItem("Undo");
		jmEditRedo = new JMenuItem("Redo");
		jmEditResize = new JMenuItem("Resize");
		jmEditPackResize = new JMenuItem("Pack Resize");

		jmEdit.add(jmEditUndo);
		jmEditUndo.addActionListener(this);
		jmEdit.add(jmEditRedo);
		jmEditRedo.addActionListener(this);
		jmEdit.add(jmEditResize);
		jmEditResize.addActionListener(this);
		jmEdit.add(jmEditPackResize);
		jmEditPackResize.addActionListener(this);

		// Layers buttons
		jmLayAdd = new JMenuItem("Add Layer");
		jmLayRename = new JMenuItem("Rename Layer");
		jmLayRemove = new JMenuItem("Remove Layer");

		jmLayers.add(jmLayAdd);
		jmLayAdd.addActionListener(this);
		jmLayers.add(jmLayRename);
		jmLayRename.addActionListener(this);
		jmLayers.add(jmLayRemove);
		jmLayRemove.addActionListener(this);

		// Map buttons
		jmMapColor = new JMenuItem("Background color");
		jmMapBackXor = new JCheckBoxMenuItem("Color XOR");
		jmMapBackXor.setSelected(true);
		jmMapBackXor
				.setToolTipText("Colors will be XOR'ed against the background to make them stand out.");
		jmMapSpriteColor = new JMenuItem("Sprite XOR color");
		jmMapSpriteColor
				.setToolTipText("This color will be XOR'ed together with the background to create a contrasting color.");
		jmMapPolyColor = new JMenuItem("Polygon XOR color");
		jmMapPolyColor
				.setToolTipText("This color will be XOR'ed together with the background to create a contrasting color.");
		jmMapPolyColor
				.setToolTipText("Enables your to see layers behind the active one");
		jmMapScaleLabel = new JLabel("Scale: 100%");
		jmMapScaleSlider = new JSlider(JSlider.HORIZONTAL, 10, 200, 100);

		jmMapPolyLabel = new JLabel("Polyline Thickness: "
				+ Settings.PolygonLineThickness);
		jmMapPolySlider = new JSlider(JSlider.HORIZONTAL, 1, 20,
				Settings.PolygonLineThickness);

		jmMapGrid = new JCheckBoxMenuItem("Grid");
		jmMapGrid.setSelected(true);
		jmMapOnion = new JCheckBoxMenuItem("Onionskin");
		jmMapOnion
				.setToolTipText("Enables you to see layers behind the active one");
		jmMapBLD = new JCheckBoxMenuItem("Background layers darkened");

		jmMap.add(jmMapColor);
		jmMapColor.addActionListener(this);
		jmMap.add(jmMapSpriteColor);
		jmMapSpriteColor.addActionListener(this);
		jmMap.add(jmMapPolyColor);
		jmMapPolyColor.addActionListener(this);
		jmMap.add(jmMapBackXor);
		jmMapBackXor.addActionListener(this);
		jmMap.add(jmMapGrid);
		jmMapGrid.addActionListener(this);
		jmMap.add(jmMapOnion);
		jmMapOnion.addActionListener(this);
		jmMap.add(jmMapBLD);
		jmMapBLD.addActionListener(this);
		jmMap.add(jmMapScaleLabel);
		jmMap.add(jmMapScaleSlider);
		jmMapScaleSlider.addChangeListener(this);
		jmMap.add(jmMapPolyLabel);
		jmMap.add(jmMapPolySlider);
		jmMapPolySlider.addChangeListener(this);

		// Tiles buttons
		jmTilesChange = new JMenuItem("Import tileset");
		jmTilesChange.addActionListener(this);
		jmSpritesChange = new JMenuItem("Manage spritesets");
		jmSpritesChange.addActionListener(this);

		jmTiles.add(jmTilesChange);
		jmTiles.add(jmSpritesChange);

		// Help buttons
		jmHelpInfo = new JMenuItem("Info");
		jmHelpInfo.addActionListener(this);
		jmHelp.add(jmHelpInfo);

		// Set up tooltip

		jmFileNew.setToolTipText("Creates a new map");
		jmFileOpen.setToolTipText("Loads a map");
		jmFileSave.setToolTipText("Saves your map");
		jmFileImp.setToolTipText("Imports a file");
		jmFileExp.setToolTipText("Saves as a filetype");
		jmFileExit.setToolTipText("Goodbye.");

		jmEditUndo.setToolTipText("Ctrl-Z");
		jmEditRedo.setToolTipText("Ctrl-Y");

		jmLayAdd.setToolTipText("Adds another layer to your map");
		jmLayRemove.setToolTipText("Removes the active layer");

		jmTilesChange.setToolTipText("Manage tilesets");

		jmHelpInfo.setToolTipText("Fear not, help is nigh");

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jmFileNew) {
			viewController.fireEvent(MapEvent.NEWHYBRIDMAP, null);
		} else if (e.getSource() == jmFileSave) {
			viewController.fireEvent(MapEvent.SAVEMAP, null);
		} else if (e.getSource() == jmFileOpen) {
			viewController.fireEvent(MapEvent.OPENMAP, null);
		} else if (e.getSource() == jmFileExp) {
			viewController.fireEvent(MapEvent.EXPORTMAP, null);
		} else if (e.getSource() == jmFileImp) {
			// int[] args = importPrompt();
			// viewController.fireEvent(MapEvent.IMPORTMAP, args);
		} else if (e.getSource() == jmFileAddScr) {
			ScriptInterpreter script = new ScriptInterpreter();
			script.addNewScript((String) JOptionPane.showInputDialog(null,
					"Please give a name for the new script:\n", "Script Name",
					JOptionPane.PLAIN_MESSAGE, null, null, "name"));
		} else if (e.getSource() == jmFileExit) {
			windowClosing(null);
		} else if (e.getSource() == jmEditUndo) {
			viewController.fireEvent(MapEvent.UNDO, null);
		} else if (e.getSource() == jmEditRedo) {
			viewController.fireEvent(MapEvent.REDO, null);
		} else if (e.getSource() == jmEditPackResize) {
			viewController.fireEvent(MapEvent.PACKRESIZEMAP, null);
		} else if (e.getSource() == jmEditResize) {
			viewController.fireEvent(MapEvent.RESIZEMAP, null);
		}
		// Layers events
		else if (e.getSource() == jmLayAdd) {
			viewController.fireEvent(MapEvent.ADDLAYER, null);
		} else if (e.getSource() == jmLayRemove) {
			int[] args = new int[1];
			viewController.fireEvent(MapEvent.REMOVELAYER, args);
		} else if (e.getSource() == jmMapGrid) {
			viewController.fireEvent(MapEvent.GRID, null);
		} else if (e.getSource() == jmMapOnion) {
			viewController.fireEvent(MapEvent.ONIONSKIN, null);
		} else if (e.getSource() == jmMapBLD) {
			viewController.fireEvent(MapEvent.BACKDARKENED, null);
		} else if (e.getSource() == jmMapColor) {
			viewController.fireEvent(MapEvent.BACKGROUNDCOLOR, null);
		} else if (e.getSource() == jmMapBackXor) {
			viewController.fireEvent(MapEvent.SETBACKXOR, null);
		} else if (e.getSource() == jmMapSpriteColor) {
			viewController.fireEvent(MapEvent.SPRITECOLOR, null);
		} else if (e.getSource() == jmMapPolyColor) {
			viewController.fireEvent(MapEvent.POLYGONCOLOR, null);
		} else if (e.getSource() == jmTilesChange) {
			viewController.fireEvent(MapEvent.SETTILESET, null);
		} else if (e.getSource() == jmSpritesChange) {
			viewController.fireEvent(MapEvent.SETSPRITESET, null);
		} else if (e.getSource() == jmHelpInfo) {
			JOptionPane.showMessageDialog(null, "Map Editor "
					+ Settings.EDITORVERSION, "Info",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	/**
	 * Create exitdialog when the window is closing.
	 */
	@Override
	public void windowClosing(WindowEvent e) {
		String exitm = "Sure you want to exit?";

		if (JOptionPane.showConfirmDialog(null, exitm, "Exit",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
			System.exit(0);
		} else {
			return;
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	/**
	 * Fire SETSCALE events whenever the user changes the scale of the map.
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == jmMapScaleSlider) {
			int[] args = new int[1];
			args[0] = jmMapScaleSlider.getValue();
			jmMapScaleLabel.setText("Scale: " + jmMapScaleSlider.getValue()
					+ "%");

			viewController.fireEvent(MapEvent.SETSCALE, args);
		} else if (e.getSource() == jmMapPolySlider) {
			int[] args = new int[1];
			args[0] = jmMapPolySlider.getValue();
			jmMapPolyLabel.setText("Polygon thickness: "
					+ jmMapPolySlider.getValue());

			viewController.fireEvent(MapEvent.SETPOLYTHICKNESS, args);
		}
	}
}
