package view;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import logic.Layer;
import logic.MapInterface;
import logic.PolygonLayer;
import logic.SpriteLayer;
import logic.TiledLayer;
import logic.TiledMap;
import view.ViewController.MapEvent;

/**
 * The EditPanel manages everything that happens in the center of the Editor.
 * Specifically it sets up the {@Link JScrollPane} with the {@Link
 *  MapPanel} inside and also handles events from this component. It will
 * also set up the infobar near the bottom and the layer-buttons on top of the
 * centerpanel and make sure that events to change layer are fired when these
 * buttons are pressed.
 */
public class EditPanel extends JPanel implements ActionListener, MouseListener,
		MouseMotionListener, AdjustmentListener {

	private static final long serialVersionUID = -6725841096962724175L;
	private Point brushStart;
	// Map panel
	private JScrollPane mapScrollPane;
	private MapPanel mapCanvas;

	public MapPanel getMapCanvas() {
		return mapCanvas;
	}

	private ViewController viewController;

	// Tabpanel
	private JPanel tabPanel;
	private Vector<JButton> tabBtns;

	// Infopanel
	private JPanel infoPanel;
	private JLabel infoTilePos;

	private boolean selectMode = true;
	private boolean editMode = false;
	
	/**
	 * Constructor.
	 * 
	 * @param view
	 *            - Reference to the {@Link ViewController} to be used
	 *            when firing events.
	 * @param width
	 *            - The width of the centerpanel.
	 * @param height
	 *            - The height of the centerpanel.
	 */
	public EditPanel(ViewController view, int width, int height) {
		viewController = view;
		initCenterPanel(width, height);
	}

	/**
	 * Used to update the text in the infobar at the bottom.
	 * 
	 * @param s
	 *            - The text to be displayed.
	 */
	public void UpdateInfo(String s) {
		infoTilePos.setText(s);
	}

	/**
	 * Use to change the backgroundcolor of the {@Link MapPanel}
	 * 
	 * @param color
	 *            - {@Link Color} to be used.
	 */
	/**
	 * Refreshes the layerbuttons after changes have been made to the map.
	 * 
	 * @param layers
	 *            - The number of layers.
	 * @param layerNum
	 *            - The index of the current layer.
	 */
	private void refreshTabPanel(Vector<Object> layers, int layerNum) {
		if (layerNum >= layers.size() || layerNum < 0)
			layerNum = layers.size() - 1;

		mapCanvas.setLayerNum(layerNum);
		tabBtns.removeAllElements();

		for (int i = 0; i < layers.size(); i++) {
			JButton b;
			if (layers.get(i) instanceof Layer)
				b = new JButton(((Layer) layers.get(i)).getName());
			else
				b = new JButton("Layer " + (i + 1));
			b.setName(Integer.toString(i));
			b.addActionListener(this);
			tabBtns.add(b);
		}
		int numBtns = tabBtns.size();
		GridLayout gl;
		gl = new GridLayout(1, numBtns);

		tabPanel.setLayout(gl);
		tabPanel.removeAll();

		for (JButton b : tabBtns) {
			tabPanel.add(b);
			if (layerNum == Integer.parseInt(b.getName()))
				b.setBackground(Color.WHITE);
			else
				b.setBackground(Color.LIGHT_GRAY);
		}
	}

	/**
	 * Method to resize a {@Link MapInterface}.
	 * 
	 * @param map
	 *            - Current {@Link MapInterface}.
	 */
	public void resizeMap(MapInterface map) {

		mapScrollPane
				.getViewport()
				.setViewSize(
						new Dimension(
								Settings.CurrentMapWidth
										* (int) (Settings.CurrentTileWidth * Settings.CurrentScale),
								Settings.CurrentMapHeight
										* (int) (Settings.CurrentTileHeight * Settings.CurrentScale)));

		mapScrollPane.getVerticalScrollBar().setUnitIncrement(
				Settings.CurrentTileHeight);
		mapScrollPane.getHorizontalScrollBar().setUnitIncrement(
				Settings.CurrentTileWidth);

	}

	/**
	 * Method initializes all components of the centerpanel.
	 * 
	 * @param frameWidth
	 *            - The width of the editor.
	 * @param framHeight
	 *            - The height of the editor.
	 */
	private void initCenterPanel(int frameWidth, int frameHeight) {
		this.setLayout(new BorderLayout());
		setPreferredSize(new Dimension(frameWidth / 2, frameHeight));

		tabPanel = new JPanel();
		add(tabPanel, BorderLayout.NORTH);
		tabPanel.setBorder(Settings.COMPBORDER);
		tabPanel.setPreferredSize(new Dimension(frameWidth / 2, 25));
		tabBtns = new Vector<JButton>();

		mapCanvas = new MapPanel(null, 0, 0);
		mapScrollPane = new JScrollPane();
		mapScrollPane.getHorizontalScrollBar().addAdjustmentListener(this);
		mapScrollPane.getVerticalScrollBar().addAdjustmentListener(this);
		mapScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		mapScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		mapScrollPane.getViewport().add(mapCanvas);
		mapScrollPane.getViewport().addMouseListener(this);
		mapScrollPane.getViewport().addMouseMotionListener(this);
		add(mapScrollPane, BorderLayout.CENTER);

		infoPanel = new JPanel();

		infoPanel.setBorder(Settings.COMPBORDER);
		infoTilePos = new JLabel("Tile: ( x:0 y:0 )  Pos: ( x:0 y:0 )");
		infoPanel.add(infoTilePos);

		add(infoPanel, BorderLayout.SOUTH);
	}

	/**
	 * Method used when a new map has been created. It sets a reference to the
	 * new map and the new tileset in the member {@Link MapPanel} of this
	 * instance.
	 * 
	 * @param map
	 *            - The new {@Link MapInterface}
	 * @param tileSet
	 *            - The new tileset.
	 */
	public void initNewMap(MapInterface map) {
		mapScrollPane
				.getViewport()
				.setViewSize(
						new Dimension(
								Settings.CurrentMapWidth
										* (int) (Settings.CurrentTileWidth * Settings.CurrentScale),
								Settings.CurrentMapHeight
										* (int) (Settings.CurrentTileHeight * Settings.CurrentScale)));

		mapCanvas.setMap(map);
		mapCanvas.setLayerNum(((TiledMap) map).getLayers().size() - 1);
		refreshTabPanel(((TiledMap) map).getLayers(), ((TiledMap) map)
				.getLayers().size() - 1);

		mapScrollPane.getVerticalScrollBar().setUnitIncrement(
				Settings.CurrentTileHeight);
		mapScrollPane.getHorizontalScrollBar().setUnitIncrement(
				Settings.CurrentTileWidth);
	}

	/**
	 * Method used when a new map has been created.
	 * 
	 * @param map
	 *            - The new {@Link MapInterface}
	 * @param tileSet
	 *            - The new tileset.
	 */
	public void updatePanel(int currentLayer, int currentPoly, int currentSprite) {
		mapCanvas.setPolyNum(currentPoly);
		mapCanvas.setSpriteNum(currentSprite);
		mapCanvas.setLayerNum(currentLayer);
		if (mapCanvas.getMap() instanceof TiledMap) {
			refreshTabPanel(((TiledMap) mapCanvas.getMap()).getLayers(),
					currentLayer);
			mapCanvas
					.setPreferredSize(new Dimension(
							(int) (Settings.CurrentMapWidth
									* Settings.CurrentTileWidth
									* Settings.CurrentScale + Settings.CurrentTileWidth
									* Settings.CurrentScale),
							(int) (Settings.CurrentMapHeight
									* Settings.CurrentTileHeight
									* Settings.CurrentScale + Settings.CurrentTileHeight
									* Settings.CurrentScale)));
		}
		mapCanvas
				.setPreferredSize(new Dimension(
						Settings.CurrentMapWidth
								* (int) (Settings.CurrentTileWidth
										* Settings.CurrentScale + (int) (Settings.CurrentTileWidth * Settings.CurrentScale)),
						Settings.CurrentMapHeight
								* (int) (Settings.CurrentTileHeight * Settings.CurrentScale)
								+ (int) (Settings.CurrentTileHeight * Settings.CurrentScale)));

		mapScrollPane
				.getViewport()
				.setViewSize(
						new Dimension(
								Settings.CurrentMapWidth
										* (int) (Settings.CurrentTileWidth
												* Settings.CurrentScale + (int) (Settings.CurrentTileWidth * Settings.CurrentScale)),
								Settings.CurrentMapHeight
										* (int) (Settings.CurrentTileHeight * Settings.CurrentScale)
										+ (int) (Settings.CurrentTileHeight * Settings.CurrentScale)));
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (mapCanvas.getMap() == null)
			return;

		int x = (mapCanvas.getScreenOffsetX() + e.getX());
		int y = (mapCanvas.getScreenOffsetY() + e.getY());

		int[] args = new int[2];
		args[0] = x;
		args[1] = y;
		viewController.fireEvent(MapEvent.UPDATEINFO, args);

		mapCanvas.setMousePos(e.getPoint());
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (mapCanvas.getMap() == null)
			return;

		int x = e.getX();
		if (x < 0 || x > mapCanvas.getWidth())
			return;
		int y = e.getY();
		if (y < 0 || y > mapCanvas.getWidth())
			return;

		if (mapCanvas.isSelecting()) {
			x = mapCanvas.getScreenOffsetX() + x;
			y = mapCanvas.getScreenOffsetY() + y;
			mapCanvas.repaint();
			Rectangle r = null;
			if (brushStart.x > x) {
				if (brushStart.y > y)
					r = new Rectangle(x, y, brushStart.x - x, brushStart.y - y);
				else
					r = new Rectangle(x, brushStart.y, brushStart.x - y, y
							- brushStart.y);
			} else if (brushStart.x <= x) {
				if (brushStart.y >= y)
					r = new Rectangle(brushStart.x, y, x - brushStart.x,
							brushStart.y - y);
				else
					r = new Rectangle(brushStart.x, brushStart.y, x
							- brushStart.x, y - brushStart.y);
			}
			mapCanvas.setSelectionRect(r);
		} else if (mapCanvas.isPlacingTile()) {
			int[] args = new int[3];

			args[0] = (mapCanvas.getScreenOffsetX() + x);
			args[1] = (mapCanvas.getScreenOffsetY() + y);
			args[2] = mapCanvas.getLayerNum();
			if (!mapCanvas.isDeleting()) {

				mapCanvas.setPlacingTile(true);
				mapCanvas.setSetTileX(args[0]);
				mapCanvas.setSetTileY(args[1]);
				viewController.fireEvent(MapEvent.SETTILE, args);
			} else if (mapCanvas.isDeleting()) {
				int old = mapCanvas.getOldTextureValue();
				int[] args0 = { 0 };
				viewController.fireEvent(MapEvent.SETCURRENTTILE, args0);
				mapCanvas.setSetTileX(args[0]);
				mapCanvas.setSetTileY(args[1]);
				viewController.fireEvent(MapEvent.SETTILE, args);

				args0[0] = old;
				viewController.fireEvent(MapEvent.SETCURRENTTILE, args0);
			}
		}
		else if(mapCanvas.isMovingPoint())
		{
			x = mapCanvas.getScreenOffsetX() + x;
			y = mapCanvas.getScreenOffsetY() + y;
			mapCanvas.repaint();

			int[] args = new int[3];
			args[0] = mapCanvas.getPointNum();
			args[1] = x;
			args[2] = y;
			viewController.fireEvent(MapEvent.MOVEPOINT, args);
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (mapCanvas.getMap() == null)
			return;

		if (mapCanvas.isSelecting()) {
			mapCanvas.setSelecting(false);
			int x = mapCanvas.getSelectionRect().x;
			int y = mapCanvas.getSelectionRect().y;
			int w = mapCanvas.getSelectionRect().width;
			int h = mapCanvas.getSelectionRect().height;

			int[] args = new int[5];
			args[0] = x;
			args[1] = y;
			args[2] = w;
			args[3] = h;

			viewController.fireEvent(MapEvent.ADDBRUSH, args);
		} else if (mapCanvas.isPlacingTile()) {
			mapCanvas.setPlacingTile(false);
			mapCanvas.setDeleting(false);
		}
		else if(mapCanvas.isMovingPoint())
		{
			mapCanvas.setMovingPoint(false);
		}
		mapCanvas.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (mapCanvas.getMap() == null)
			return;

		if (mapCanvas.isSelecting()) {
			brushStart = new Point(e.getX() + mapCanvas.getScreenOffsetX(),
					e.getY() + mapCanvas.getScreenOffsetY());
		} else if (mapCanvas.getMap().getLayers().get(mapCanvas.getLayerNum()) instanceof TiledLayer) {
			mapCanvas.setPlacingTile(true);
			if (e.getButton() == MouseEvent.BUTTON3)
				mapCanvas.setDeleting(true);
			int[] args = new int[3];

			args[0] = (mapCanvas.getScreenOffsetX() + e.getX());
			args[1] = (mapCanvas.getScreenOffsetY() + e.getY());
			args[2] = mapCanvas.getLayerNum();

			if (e.getButton() == MouseEvent.BUTTON1) {
				viewController.fireEvent(MapEvent.SETTILE, args);
				mapCanvas.setPlacingTile(true);
				mapCanvas.setSetTileX(args[0]);
				mapCanvas.setSetTileY(args[1]);
			} else if (e.getButton() == MouseEvent.BUTTON3) {
				int[] args0 = { 0 };
				viewController.fireEvent(MapEvent.SETCURRENTTILE, args0);

				viewController.fireEvent(MapEvent.SETTILE, args);
				mapCanvas.setPlacingTile(true);
				mapCanvas.setSetTileX(args[0]);
				mapCanvas.setSetTileY(args[1]);
				args0[0] = mapCanvas.getOldTextureValue();
				viewController.fireEvent(MapEvent.SETCURRENTTILE, args0);
			}
		} else if (mapCanvas.getMap().getLayers().get(mapCanvas.getLayerNum()) instanceof PolygonLayer) {
			if (editMode && e.getButton() == MouseEvent.BUTTON3) {
				viewController
						.fireEvent(MapEvent.REMOVEPOINT, new int[] { -1 });
			} else {
				int[] args = new int[2];
				if (mapCanvas.getScreenOffsetX() + e.getX() > Settings.CurrentMapWidth
						* Settings.CurrentTileWidth * Settings.CurrentScale
						|| e.getX() < 0)
					return;
				if (mapCanvas.getScreenOffsetY() + e.getY() > Settings.CurrentMapHeight
						* Settings.CurrentTileHeight * Settings.CurrentScale
						|| e.getY() < 0)
					return;

				args[0] = mapCanvas.getScreenOffsetX() + e.getX();
				args[1] = mapCanvas.getScreenOffsetY() + e.getY();
				
				PolygonLayer layer = (PolygonLayer)mapCanvas.getMap().getLayers().get(mapCanvas.getLayerNum());
				if(editMode)
					viewController.fireEvent(MapEvent.ADDPOINT, args);
				else if(selectMode)
				{
					int nearestPolyIndex = -1;
					int nearestPointIndex = -1;
					double nearestDist = Double.MAX_VALUE;
					for(int i = 0; i< layer.size(); i++)
					{
						Vector<Point> poly = layer.getPolygon(i);
						for(int j = 0; j<poly.size(); j++)
						{
							double dist = poly.get(j).distance(args[0], args[1]);
							if(dist < nearestDist)
							{
								nearestPolyIndex = i;
								nearestPointIndex = j;
								nearestDist = dist;
							}
						}
					}
					if(nearestPolyIndex > -1 && nearestPointIndex > -1)
					{
						args[0] = nearestPolyIndex;
						viewController.fireEvent(MapEvent.CHANGEPOLYGON, args);
						mapCanvas.setPointNum(nearestPointIndex);
						mapCanvas.setMovingPoint(true);
					}
				}
					
			}
		} else if (mapCanvas.getMap().getLayers().get(mapCanvas.getLayerNum()) instanceof SpriteLayer) {
			if (e.getButton() == MouseEvent.BUTTON3) {
				viewController.fireEvent(MapEvent.REMOVESPRITE,
						new int[] { -1 });
			} else {
				int[] args = new int[2];
				if (mapCanvas.getScreenOffsetX() + e.getX() > Settings.CurrentMapWidth
						* Settings.CurrentTileWidth * Settings.CurrentScale
						|| e.getX() < 0)
					return;
				if (mapCanvas.getScreenOffsetY() + e.getY() > Settings.CurrentMapHeight
						* Settings.CurrentTileHeight * Settings.CurrentScale
						|| e.getY() < 0)
					return;

				args[0] = mapCanvas.getScreenOffsetX() + e.getX();
				args[1] = mapCanvas.getScreenOffsetY() + e.getY();

				viewController.fireEvent(MapEvent.ADDSPRITE, args);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int layerNum = 0;
		for (JButton b : tabBtns) {
			if (e.getSource() == b) {
				layerNum = Integer.parseInt(b.getName());
			}
		}
		mapCanvas.setLayerNum(layerNum);
		int[] args = new int[1];
		args[0] = layerNum;
		viewController.fireEvent(MapEvent.CHANGELAYER, args);
		viewController.fireEvent(MapEvent.UPDATEVIEW, null);
	}

	@Override
	public void adjustmentValueChanged(AdjustmentEvent e) {
		if (e.getValueIsAdjusting()) {
			return;
		}
		int[] args = new int[1];
		int orient = e.getAdjustable().getOrientation();
		if (orient == Adjustable.HORIZONTAL) {
			args[0] = e.getAdjustable().getValue();
			viewController.fireEvent(MapEvent.CHANGEXOFFSET, args);
		} else if (orient == Adjustable.VERTICAL) {
			args[0] = e.getAdjustable().getValue();
			viewController.fireEvent(MapEvent.CHANGEYOFFSET, args);
		}
	}

	public void setEditMode(boolean b) {
		editMode = b;
	}
	
	public void setSelectMode(boolean b) {
		selectMode = b;
	}
}
