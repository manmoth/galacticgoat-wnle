package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import logic.HybridMap;
import logic.PolygonLayer;
import view.ViewController.MapEvent;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.vividsolutions.jts.operation.polygonize.Polygonizer;

/**
 * The PolyPanel class provides tools for working with polygons.
 */
public class PolyPanel extends JPanel implements ActionListener {

	class PointModel implements ListModel {
		Vector<Point> data;

		public PointModel(Vector<Point> data) {
			this.data = data;
		}

		public Object getElementAt(int index) {
			return new String("Point " + index + " - X: "
					+ data.elementAt(index).x + " Y: "
					+ data.elementAt(index).y);
		}

		public int getSize() {
			return data.size();
		}

		@Override
		public void addListDataListener(ListDataListener arg0) {
		}

		@Override
		public void removeListDataListener(ListDataListener arg0) {
		}
	}

	private static final long serialVersionUID = 616534228650134002L;
	private ViewController viewController;
	private Vector<Point> currentPoints;

	public Vector<Point> getCurrentPoints() {
		return currentPoints;
	}

	public void setCurrentPoints(Vector<Point> currentPoints) {
		this.currentPoints = currentPoints;
	}

	private Vector<JRadioButton> buttons;
	private JPanel buttonPanel;
	private JPanel selectPanel;
	private JScrollPane pointScrollPane;
	private JScrollPane selectScrollPane;
	private JList pointList;
	private JButton start;
	private JButton inflate;
	private JButton remove;
	private JButton undo;
	private JButton end;
	private JCheckBox snap;
	private boolean snapToGrid;
	private JCheckBox select;
	
	public void addPoint(Point p) {
		if (snapToGrid) {
			int x = p.x;
			int y = p.y;

			if (x % Settings.CurrentTileWidth > Settings.CurrentTileWidth / 2)
				x = (x / Settings.CurrentTileWidth) * Settings.CurrentTileWidth
						+ Settings.CurrentTileWidth;
			else
				x = (x / Settings.CurrentTileWidth) * Settings.CurrentTileWidth;

			if (y % Settings.CurrentTileHeight > Settings.CurrentTileHeight / 2)
				y = (y / Settings.CurrentTileHeight)
						* Settings.CurrentTileHeight
						+ Settings.CurrentTileHeight;
			else
				y = (y / Settings.CurrentTileHeight)
						* Settings.CurrentTileHeight;
			p = new Point(x, y);
		}

		currentPoints.add(p);
	}
	
	public void movePoint(int index, Point p) {
		
		if (snapToGrid) {
			int x = p.x;
			int y = p.y;

			if (x % Settings.CurrentTileWidth > Settings.CurrentTileWidth / 2)
				x = (x / Settings.CurrentTileWidth) * Settings.CurrentTileWidth
						+ Settings.CurrentTileWidth;
			else
				x = (x / Settings.CurrentTileWidth) * Settings.CurrentTileWidth;

			if (y % Settings.CurrentTileHeight > Settings.CurrentTileHeight / 2)
				y = (y / Settings.CurrentTileHeight)
						* Settings.CurrentTileHeight
						+ Settings.CurrentTileHeight;
			else
				y = (y / Settings.CurrentTileHeight)
						* Settings.CurrentTileHeight;
			p = new Point(x, y);
		}
		
		currentPoints.set(index, p);
	}

	public void inflate(int offset) {
		Coordinate[] coords = new Coordinate[currentPoints.size()];
		for (int i = 0; i < currentPoints.size(); i++) {
			coords[i] = new Coordinate(currentPoints.get(i).x,
					currentPoints.get(i).y);
		}
		Geometry poly = new GeometryFactory().createLineString(coords);
		BufferOp buffer = new BufferOp(poly);
		buffer.setEndCapStyle(BufferParameters.CAP_FLAT);
		poly = buffer.getResultGeometry(offset);
		currentPoints.clear();
		coords = poly.getCoordinates();
		for (int i = 0; i < coords.length; i++) {
			if (i - 1 > 0)
				if (currentPoints.lastElement().x == (int) coords[i].x
						&& currentPoints.lastElement().y == (int) coords[i].y) {
					continue;
				}
			currentPoints.add(new Point((int) coords[i].x, (int) coords[i].y));
		}
	}

	public void removePoint(int i) {
		if (i >= 0 && i < currentPoints.size())
			currentPoints.remove(i);
	}

	/**
	 * Contstructor. Sets up the polypanel.
	 * 
	 * @param ViewController
	 *            - The {@Link ViewController} to be used when firing
	 *            events.
	 * @param frameWidth
	 *            - The width of the editor.
	 * @param framHeight
	 *            - The height of the editor.
	 */
	public PolyPanel(ViewController viewController, int frameWidth,
			int frameHeight) {
		snapToGrid = false;
		this.viewController = viewController;
		currentPoints = new Vector<Point>();
		buttons = new Vector<JRadioButton>();
		pointList = new JList();
		pointList.setModel(new PointModel(currentPoints));

		pointScrollPane = new JScrollPane();
		pointScrollPane.getViewport().add(pointList);
		pointScrollPane.setPreferredSize(new Dimension(frameWidth / 4,
				frameHeight / 3));
		pointScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pointScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pointScrollPane.setViewportBorder(Settings.COMPBORDER);

		selectPanel = new JPanel();
		selectScrollPane = new JScrollPane();
		selectScrollPane.getViewport().add(selectPanel);
		selectScrollPane.setPreferredSize(new Dimension(frameWidth / 4,
				frameHeight / 6));
		selectScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		selectScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		selectScrollPane.setViewportBorder(Settings.COMPBORDER);

		buttonPanel = new JPanel(new GridLayout(8, 1, 0, 2));

		JLabel jl = new JLabel("<html><u>Polygon Tools</u></html>");
		start = new JButton("New Polygon");
		start.addActionListener(this);
		start.setToolTipText("Start specifying a new polygon. Will discard the current polygon if it is not saved.");
		start.setBackground(Color.LIGHT_GRAY);
		inflate = new JButton("Inflate");
		inflate.addActionListener(this);
		inflate.setToolTipText("Will inflate the polygon by the specified amount.");
		inflate.setBackground(Color.LIGHT_GRAY);
		undo = new JButton("Undo Point");
		undo.addActionListener(this);
		undo.setToolTipText("Remove the most recently specified point.");
		undo.setBackground(Color.LIGHT_GRAY);
		end = new JButton("Save Polygon");
		end.addActionListener(this);
		end.setToolTipText("Save the polygon.");
		end.setBackground(Color.LIGHT_GRAY);
		remove = new JButton("Remove Polygon");
		remove.addActionListener(this);
		remove.setToolTipText("Remove the selected polygon.");
		remove.setBackground(Color.LIGHT_GRAY);

		snap = new JCheckBox("Snap to grid");
		snap.addActionListener(this);
		select = new JCheckBox("Select mode");
		select.addActionListener(this);

		buttonPanel.add(jl);
		buttonPanel.add(start);
		buttonPanel.add(inflate);
		buttonPanel.add(undo);
		buttonPanel.add(end);
		buttonPanel.add(remove);
		buttonPanel.add(snap);
		buttonPanel.add(select);

		add(buttonPanel);
		add(pointScrollPane);
		add(selectScrollPane);

		setPreferredSize(new Dimension(frameWidth / 3, frameHeight));
	}

	public void refreshPolyPanel(HybridMap hybridMap, int layerNum) {

		selectPanel.removeAll();
		selectPanel.setLayout(new GridLayout(buttons.size(), 1));
		selectPanel.setPreferredSize(new Dimension(selectScrollPane
				.getViewport().getSize().width - 10, buttons.size() * 20));

		int selectedIndex = 0;
		for (JRadioButton b : buttons)
			if (b.isSelected())
				selectedIndex = buttons.indexOf(b);

		buttons.removeAllElements();
		ButtonGroup group = new ButtonGroup();

		if (hybridMap.getLayer(layerNum) instanceof PolygonLayer)
			for (int i = 0; i < ((PolygonLayer) hybridMap.getLayer(layerNum))
					.size(); i++) {
				JRadioButton b = new JRadioButton(
						String.valueOf(buttons.size()));
				b.addActionListener(this);
				group.add(b);
				buttons.add(b);
				selectPanel.add(b);
			}
		if (buttons.size() > 0)
			buttons.get(selectedIndex).setSelected(true);
		
		pointList.setModel(new PointModel(currentPoints));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton) {
			for (int i = 0; i < buttons.size(); i++)
				if (buttons.get(i) == e.getSource()) {
					int[] args = new int[1];
					args[0] = i;
					viewController.fireEvent(MapEvent.CHANGEPOLYGON, args);
					break;
				}
		} else if (e.getSource() == remove) {
			int[] args = new int[1];
			args[0] = -1;
			for (int i = 0; i < buttons.size(); i++)
				if (buttons.get(i).isSelected()) {
					args[0] = i;
					buttons.remove(i);
					for (int j = 0; j < buttons.size(); j++)
						buttons.get(j).setText(String.valueOf(j));
					if (buttons.size() > 0)
						buttons.lastElement().setSelected(true);
					break;
				}
			viewController.fireEvent(MapEvent.REMOVEPOLYGON, args);
		} else if (e.getSource() == undo) {
			if (currentPoints.size() > 0)
				currentPoints.remove(currentPoints.size() - 1);
		} else if (e.getSource() == inflate) {
			viewController.fireEvent(MapEvent.INFLATE, null);
		} else if (e.getSource() == start || e.getSource() == end) {
			currentPoints = new Vector<Point>();
			viewController.fireEvent(MapEvent.ADDPOLYGON, null);
			currentPoints.clear();
		} else if (e.getSource() == snap) {
			snapToGrid = snap.isSelected();
		} else if (e.getSource() == select) {
			if(select.isSelected())
				viewController.fireEvent(MapEvent.EDITMODE, null);
			else
				viewController.fireEvent(MapEvent.SELECTMODE, null);
		}
	}
}
