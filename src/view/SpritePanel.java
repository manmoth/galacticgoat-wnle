package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import logic.Sprite;
import view.ViewController.MapEvent;

/**
 * The SpritePanel provides tools for working with sprites.
 */
public class SpritePanel extends JPanel implements ActionListener,
		ListSelectionListener {
	class SpriteModel implements ListModel {
		Vector<Sprite> data;

		public SpriteModel(Vector<Sprite> data) {
			this.data = data;
		}

		public Object getElementAt(int index) {
			return new String("Sprite " + index + " - X: "
					+ data.elementAt(index).getPos().x + " Y: "
					+ data.elementAt(index).getPos().y);
		}

		public int getSize() {
			return data.size();
		}

		@Override
		public void addListDataListener(ListDataListener arg0) {
		}

		@Override
		public void removeListDataListener(ListDataListener arg0) {
		}
	}

	private static final long serialVersionUID = -1992600598162221190L;
	private ViewController viewController;
	private JPanel spritePanel;
	private Vector<JButton> spriteBtns;
	private Vector<BufferedImage> spriteIcons;
	private JScrollPane buttonScrollPane;
	private JList spriteList;
	private JScrollPane spriteScrollPane;
	private JPanel buttonPanel;
	private JButton remove;
	private JCheckBox snap;
	private boolean snapToGrid;

	public boolean isSnapToGrid() {
		return snapToGrid;
	}

	/**
	 * Constructor.
	 * 
	 * @param ViewController
	 *            - The {@Link ViewController} to be used when firing
	 *            events.
	 * @param frameWidth
	 *            - The width of the editor.
	 * @param framHeight
	 *            - The height of the editor.
	 * 
	 * */
	public SpritePanel(ViewController viewController, int frameWidth,
			int frameHeight) {
		this.viewController = viewController;
		spritePanel = new JPanel();
		setBorder(Settings.COMPBORDER);
		setPreferredSize(new Dimension(frameWidth / 3, frameHeight));
		buttonScrollPane = new JScrollPane();
		buttonScrollPane.setPreferredSize(new Dimension(frameWidth / 4,
				frameHeight / 3));
		buttonScrollPane.getViewport().add(spritePanel);

		buttonScrollPane.getVerticalScrollBar().setUnitIncrement(
				Settings.CurrentTileHeight);
		buttonScrollPane.getHorizontalScrollBar().setUnitIncrement(
				Settings.CurrentTileWidth);
		buttonScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		buttonScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		spriteList = new JList();
		spriteList.setModel(new SpriteModel(new Vector<Sprite>()));
		spriteList.addListSelectionListener(this);

		spriteScrollPane = new JScrollPane();
		spriteScrollPane.getViewport().add(spriteList);
		spriteScrollPane.setPreferredSize(new Dimension(frameWidth / 4,
				frameHeight / 3));
		spriteScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		spriteScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		spriteScrollPane.setViewportBorder(Settings.COMPBORDER);

		buttonPanel = new JPanel(new GridLayout(5, 2, 0, 2));

		JLabel jl = new JLabel("<html><u>Sprite Tools</u></html>");
		remove = new JButton("Remove Sprite");
		remove.addActionListener(this);
		remove.setToolTipText("Remove the selected sprite.");
		remove.setBackground(Color.LIGHT_GRAY);
		snap = new JCheckBox("Snap to grid");
		snap.addActionListener(this);

		buttonPanel.add(jl);
		buttonPanel.add(remove);
		buttonPanel.add(snap);

		add(buttonPanel);
		add(buttonScrollPane);
		add(spriteScrollPane);
		spriteIcons = new Vector<BufferedImage>();
	}

	public void refreshSpritePanel(int currentSprite, int frameWidth,
			int layerNum, Vector<BufferedImage> spriteSet,
			Vector<Sprite> sprites) {
		spriteIcons.removeAllElements();
		if (spriteSet != null)
			for (BufferedImage sprite : spriteSet) {
				BufferedImage scaledImg = new BufferedImage(
						Settings.CurrentTileWidth, Settings.CurrentTileHeight,
						sprite.getType());
				AffineTransform tx = new AffineTransform();
				tx.scale(Settings.CurrentTileWidth / (float) sprite.getWidth(),
						Settings.CurrentTileHeight / (float) sprite.getHeight());
				AffineTransformOp op = new AffineTransformOp(tx,
						AffineTransformOp.TYPE_BILINEAR);
				op.filter(sprite, scaledImg);
				spriteIcons.add(scaledImg);
			}
		spritePanel.removeAll();

		spriteBtns = new Vector<JButton>();
		for (int i = 0; i < spriteIcons.size(); i++) {
			JButton tbtn = new JButton(new ImageIcon(spriteIcons.elementAt(i)));
			if (i == currentSprite)
				tbtn.setBackground(Color.BLACK);
			tbtn.setPreferredSize(new Dimension(Settings.CurrentTileWidth,
					Settings.CurrentTileHeight));
			int width = (Settings.CurrentTileWidth * 2) + 20;
			if (width < frameWidth / 3)
				buttonScrollPane.getViewport().setPreferredSize(
						new Dimension(width, getHeight()));
			else
				buttonScrollPane.getViewport().setPreferredSize(
						new Dimension(frameWidth / 4, getHeight()));
			tbtn.setName(Integer.toString(i));
			tbtn.addActionListener(this);
			spriteBtns.add(tbtn);
		}

		GridLayout gl;

		gl = new GridLayout((int) Math.ceil((spriteBtns.size() + 1) / 2.0), 2);

		spritePanel.setLayout(gl);

		for (JButton b : spriteBtns)
			spritePanel.add(b);

		if (sprites != null)
			spriteList.setModel(new SpriteModel(sprites));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == remove) {

			int[] args = new int[1];
			args[0] = spriteList.getSelectedIndex();

			viewController.fireEvent(MapEvent.REMOVESPRITE, args);
		} else if (e.getSource() == snap) {
			snapToGrid = snap.isSelected();
		} else {
			int[] args = new int[1];
			for (JButton b : spriteBtns)
				if (e.getSource() == b)
					args[0] = Integer.parseInt(b.getName());

			viewController.fireEvent(MapEvent.SETCURRENTSPRITE, args);
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		int[] args = new int[1];
		args[0] = spriteList.getSelectedIndex();
		viewController.fireEvent(MapEvent.CHANGESPRITE, args);
	}

}
