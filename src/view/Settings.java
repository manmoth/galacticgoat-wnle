package view;

import java.awt.Color;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class Settings {

	public final static String EDITORVERSION = "v0.5";

	public final static int MAXWIDTH = 2000;
	public final static int MAXHEIGHT = 2000;
	public final static int MAXTILEWIDTH = 2048;
	public final static int MAXTILEHEIGHT = 2048;
	public final static int MINTILEWIDTH = 8;
	public final static int MINTILEHEIGHT = 8;
	public final static int MAXSPRITEWIDTH = 2048;
	public final static int MAXSPRITEHEIGHT = 2048;
	public final static int MINSPRITEWIDTH = 8;
	public final static int MINSPRITEHEIGHT = 8;

	public final static Border COMPBORDER = new LineBorder(Color.GRAY);

	public static int CurrentTileWidth = 32;
	public static int CurrentTileHeight = 32;
	public static int CurrentMapWidth = 10;
	public static int CurrentMapHeight = 10;
	public static int PolygonLineThickness = 4;

	public static float CurrentScale = 1;
}
