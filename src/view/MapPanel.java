package view;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.Map;
import java.util.Vector;

import javax.swing.JPanel;

import logic.HybridMap;
import logic.MapInterface;
import logic.PolygonLayer;
import logic.Sprite;
import logic.SpriteLayer;
import logic.TiledLayer;
import logic.TiledMap;
import data.SpriteSet;
import data.TileSet;

/**
 * The MapPanel class is used to draw the map and contains special methods used
 * for this purpose.
 */
public class MapPanel extends JPanel {

	private static final long serialVersionUID = 6796505094820613508L;
	private MapInterface map;

	public MapInterface getMap() {
		return map;
	}

	public void setMap(MapInterface map) {
		this.map = map;
	}

	private int layerNum;

	public int getLayerNum() {
		return layerNum;
	}

	public void setLayerNum(int layerNum) {
		this.layerNum = layerNum;
	}

	private int polyNum;

	public int getPolyNum() {
		return polyNum;
	}

	public void setPolyNum(int polyNum) {
		this.polyNum = polyNum;
	}
	
	private int pointNum;

	public int getPointNum() {
		return pointNum;
	}

	public void setPointNum(int pointNum) {
		this.pointNum = pointNum;
	}


	private int spriteNum;

	public int getSpriteNum() {
		return spriteNum;
	}

	public void setSpriteNum(int spriteNum) {
		this.spriteNum = spriteNum;
	}

	private Map<String, SpriteSet> spriteSets;

	public Map<String, SpriteSet> getSpriteSets() {
		return spriteSets;
	}

	/**
	 * Use to set the mapCanvas spriteset.
	 */
	public void setSpriteSets(Map<String, SpriteSet> spriteSets) {
		this.spriteSets = spriteSets;
	}

	private Map<String, TileSet> tileSets;

	public Map<String, TileSet> getTileSets() {
		return tileSets;
	}

	/**
	 * Use to set the mapCanvas tileset.
	 */
	public void setTileSets(Map<String, TileSet> tileSets) {
		this.tileSets = tileSets;
	}

	private float previousTileScaleFactor = 1.0f;
	private Vector<BufferedImage> scaledTileImages;
	private float previousSpriteScaleFactor = 1.0f;
	private Vector<BufferedImage> scaledSpriteImages;
	private int screenOffsetX;

	public int getScreenOffsetX() {
		return screenOffsetX;
	}

	/**
	 * Set the screenOffset X value.
	 * 
	 * @param x
	 *            - new screenOffset value.
	 */
	public void setScreenOffsetX(int screenOffsetX) {
		this.screenOffsetX = screenOffsetX;
	}

	private int screenOffsetY;

	public int getScreenOffsetY() {
		return screenOffsetY;
	}

	/**
	 * Set the screenOffset Y value.
	 * 
	 * @param y
	 *            - new screenOffset value.
	 */
	public void setScreenOffsetY(int screenOffsetY) {
		this.screenOffsetY = screenOffsetY;
	}

	private Color background;

	public void setBackgroundColor(Color background) {
		this.background = background;
	}

	private Color polygon;

	/**
	 * Use to change the polygoncolor of the {@Link MapPanel}
	 * 
	 * @param color
	 *            - {@Link Color} to be used.
	 */
	public void setPolygonColor(Color polygon) {
		this.polygon = polygon;
	}

	private Color sprite;

	/**
	 * Use to change the outlinecolor of the {@Link MapPanel}
	 * 
	 * @param color
	 *            - {@Link Color} to be used.
	 */
	public void setSpriteColor(Color sprite) {
		this.sprite = sprite;
	}

	/**
	 * Get the backgroundcolor of the {@Link MapPanel}
	 * 
	 * @return The color of the mapbackground.
	 */
	public Color getBackgroundColor() {
		return background;
	}

	/**
	 * Get the polygoncolor of the {@Link MapPanel}
	 * 
	 * @return The color of the polylines.
	 */
	public Color getPolygonColor() {
		return polygon;
	}

	/**
	 * Get the outlinecolor of the {@Link MapPanel}
	 * 
	 * @return The color of the spriteoutline.
	 */
	public Color getSpriteColor() {
		return sprite;
	}

	private boolean displayGrid = true;

	public boolean isGrid() {
		return displayGrid;
	}

	/**
	 * Set whether or not the grid is displayed.
	 */
	public void setGrid(boolean grid) {
		this.displayGrid = grid;
	}

	private boolean onionSkin = false;

	public boolean isOnionskin() {
		return onionSkin;
	}

	/**
	 * Set whether or not the layers are to be drawn onionskinned.
	 */
	public void setOnionskin(boolean onionskin) {
		this.onionSkin = onionskin;
	}

	private boolean backXor = false;

	public boolean isBackXor() {
		return backXor;
	}

	/**
	 * Set whether or not colors are contrasted with the background.
	 */
	public void setBackXor(boolean backXor) {
		this.backXor = backXor;
	}

	private boolean backDarkened = false;

	public boolean isBackDarkened() {
		return backDarkened;
	}

	/**
	 * Set whether or not background layers are darkened when drawing
	 * onionskinned.
	 */
	public void setBackDarkened(boolean backdarkened) {
		this.backDarkened = backdarkened;
	}

	private boolean setTile = false;

	public boolean isSetTile() {
		return setTile;
	}

	public void setSetTile(boolean setTile) {
		this.setTile = setTile;
	}

	private int setTileX = 0;

	public int getSetTileX() {
		return setTileX;
	}

	public void setSetTileX(int setTileX) {
		this.setTileX = setTileX;
	}

	private int setTileY = 0;

	public int getSetTileY() {
		return setTileY;
	}

	public void setSetTileY(int setTileY) {
		this.setTileY = setTileY;
	}

	private boolean selecting = false;

	public boolean isSelecting() {
		return selecting;
	}

	public void setSelecting(boolean selection) {
		this.selecting = selection;
	}
	
	private boolean movingPoint = false;

	public boolean isMovingPoint() {
		return movingPoint;
	}

	public void setMovingPoint(boolean moving) {
		this.movingPoint = moving;
	}

	private Point mousePos;

	public void setMousePos(Point mousePos) {
		this.mousePos = mousePos;
	}

	private Rectangle selectionRect;

	public Rectangle getSelectionRect() {
		return selectionRect;
	}

	public void setSelectionRect(Rectangle selectionRect) {
		this.selectionRect = selectionRect;
	}

	private boolean placingTile = false;

	public boolean isPlacingTile() {
		return placingTile;
	}

	public void setPlacingTile(boolean tile) {
		this.placingTile = tile;
	}

	private boolean deleting = false;

	public boolean isDeleting() {
		return deleting;
	}

	public void setDeleting(boolean delete) {
		this.deleting = delete;
	}

	private boolean polygonSelect = false;

	public boolean isPolygonSelect() {
		return polygonSelect;
	}

	public void setPolygonSelect(boolean poly) {
		this.polygonSelect = poly;
	}
	
	private int oldTextureValue = 0;

	public void setOldTextureValue(int oldTextureValue) {
		this.oldTextureValue = oldTextureValue;
	}

	public int getOldTextureValue() {
		return oldTextureValue;
	}

	private BufferedImage bi;

	/**
	 * Constructor.
	 * 
	 * @param map
	 *            - The map to be drawn to screen.
	 * @param xPosition
	 *            - the starting x-position of the upper left corner of the
	 *            screen.
	 * @param yPosition
	 *            - the starting y-position of the upper left corner of the
	 *            screen.
	 */
	public MapPanel(MapInterface map, int xPosition, int yPosition) {
		scaledTileImages = new Vector<BufferedImage>();
		scaledSpriteImages = new Vector<BufferedImage>();
		backXor = true;
		background = Color.BLACK;
		sprite = Color.BLUE;
		polygon = Color.RED;
		mousePos = new Point(0, 0);
		this.map = map;
		this.screenOffsetX = xPosition;
		this.screenOffsetY = yPosition;
		bi = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
	}

	/**
	 * paintComponent draws the map to screen.
	 * 
	 * @param g
	 *            - The Graphics object that is used to draw.
	 */
	public void paintComponent(Graphics g) {
		if (map != null) {
			oldTextureValue = ((TiledMap) getMap()).getCurrentTex();
			Graphics2D g2d = ((Graphics2D) g);
			TiledMap tiledMap = (TiledMap) map;
			if (tiledMap.getLayers().size() == 0 || layerNum < 0)
				return;

			g2d.setColor(background);
			g2d.fillRect(screenOffsetX, screenOffsetY, getWidth(), getHeight());

			g2d.setColor(Color.GRAY);
			g2d.setStroke(new BasicStroke(10));
			g2d.drawLine(
					0,
					(int) (Settings.CurrentMapHeight
							* Settings.CurrentTileHeight * Settings.CurrentScale) + 5,
					(int) (Settings.CurrentMapWidth * Settings.CurrentTileWidth * Settings.CurrentScale),
					(int) (Settings.CurrentMapHeight
							* Settings.CurrentTileHeight * Settings.CurrentScale) + 5);
			g2d.drawLine(
					(int) (Settings.CurrentMapWidth * Settings.CurrentTileWidth * Settings.CurrentScale) + 5,
					0,
					(int) (Settings.CurrentMapWidth * Settings.CurrentTileWidth
							* Settings.CurrentScale + 5),
					(int) (Settings.CurrentMapHeight
							* Settings.CurrentTileHeight * Settings.CurrentScale));

			if (isOnionskin())
				drawOnionSkinned(g2d, map);
			else
				drawSingle(g2d, map);
			if (isSelecting()) {
				Composite normal = g2d.getComposite();
				g2d.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, 0.2f));
				g2d.setColor(Color.YELLOW);
				g2d.setStroke(new BasicStroke(5));
				if (selectionRect != null)
					g2d.draw(selectionRect);
				g2d.setComposite(normal);
			}
			if (map.getLayer(layerNum) instanceof SpriteLayer) {
				g2d.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, 0.4f));
				drawSprite(
						g2d,
						new Sprite(
								((HybridMap) map).getCurrentSpriteTex(),
								new Point(
										screenOffsetX
												+ (int) (mousePos.x / Settings.CurrentScale),
										screenOffsetY
												+ (int) (mousePos.y / Settings.CurrentScale)),
								null),
						((SpriteLayer) map.getLayer(layerNum)).getSpriteSetId(),
						false, false);
			} else if (map.getLayer(layerNum) instanceof TiledLayer) {
				g2d.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, 0.4f));
				drawSingleTile(
						g2d,
						((TiledLayer) map.getLayer(layerNum)).getTileSetId(),
						((TiledMap) map).getCurrentTex(),
						(int) ((screenOffsetX + mousePos.x) / (Settings.CurrentTileWidth * Settings.CurrentScale)),
						(int) ((screenOffsetY + mousePos.y) / (Settings.CurrentTileHeight * Settings.CurrentScale)),
						false);
			}
			if (displayGrid) {
				g2d.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, 0.6f));
				drawGrid(g2d);
			}
		}
	}

	/**
	 * Used to draw the grid.
	 * 
	 * @param g2d
	 *            - Graphics object.
	 */
	private void drawGrid(Graphics2D g2d) {
		g2d.setColor(new Color(background.getRGB() ^ Color.WHITE.getRGB()));
		g2d.setStroke(new BasicStroke(1));

		for (int y = 0; y < Settings.CurrentMapHeight; y++) {
			g2d.drawLine(
					0,
					(int) (y * Settings.CurrentTileHeight * Settings.CurrentScale),
					(int) (Settings.CurrentMapWidth * Settings.CurrentTileWidth * Settings.CurrentScale),
					(int) (y * Settings.CurrentTileHeight * Settings.CurrentScale));
		}
		for (int x = 0; x < Settings.CurrentMapWidth; x++) {
			g2d.drawLine(
					(int) (x * Settings.CurrentTileWidth * Settings.CurrentScale),
					0,
					(int) (x * Settings.CurrentTileWidth * Settings.CurrentScale),
					(int) (Settings.CurrentMapHeight
							* Settings.CurrentTileHeight * Settings.CurrentScale));
		}
	}

	/**
	 * Used to draw a single layer.
	 * 
	 * @param g2d
	 *            - Graphics object.
	 * @param map
	 *            - {@Link MapInterface}
	 */
	private void drawSingle(Graphics2D g2d, MapInterface map) {

		if (((TiledMap) map).getLayer(layerNum) instanceof TiledLayer) {
			drawTiledLayer(g2d,
					(TiledLayer) ((TiledMap) map).getLayer(layerNum), true);
		} else if (((TiledMap) map).getLayer(layerNum) instanceof PolygonLayer) {
			drawPolyLayer(g2d,
					(PolygonLayer) ((HybridMap) map).getLayer(layerNum), true);
		} else if (((TiledMap) map).getLayer(layerNum) instanceof SpriteLayer) {
			drawSpriteLayer(g2d,
					(SpriteLayer) ((HybridMap) map).getLayer(layerNum), true);
		}
	}

	/**
	 * Used to draw a all the layers onionskinned.
	 * 
	 * @param g2d
	 *            - Graphics object.
	 * @param map
	 *            - {@Link MapInterface}
	 */
	private void drawOnionSkinned(Graphics2D g2d, MapInterface map) {

		Vector<Object> drawLayers = new Vector<Object>();

		Object front = ((TiledMap) map).getLayer(layerNum);

		for (Object layer : ((TiledMap) map).getLayers())
			if (layer != ((TiledMap) map).getLayer(layerNum))
				drawLayers.add(layer);

		for (Object layer : drawLayers) {
			if (layer instanceof TiledLayer) {
				drawTiledLayer(g2d, (TiledLayer) layer, false);
			} else if (layer instanceof PolygonLayer) {
				drawPolyLayer(g2d, (PolygonLayer) layer, false);
			} else if (layer instanceof SpriteLayer) {
				drawSpriteLayer(g2d, (SpriteLayer) layer, false);
			}
		}

		if (front instanceof TiledLayer) {
			drawTiledLayer(g2d, (TiledLayer) front, true);
		} else if (front instanceof PolygonLayer) {
			drawPolyLayer(g2d, (PolygonLayer) front, true);
		} else if (front instanceof SpriteLayer) {
			drawSpriteLayer(g2d, (SpriteLayer) front, true);
		}
	}

	private void drawSprite(Graphics2D g2d, Sprite s, String spriteset,
			boolean selected, boolean front) {

		int startY = (int) ((screenOffsetY - Settings.CurrentTileHeight
				* Settings.CurrentScale));
		int endY = (int) ((screenOffsetY + getParent().getHeight() + Settings.CurrentTileHeight
				* Settings.CurrentScale));
		int startX = (int) ((screenOffsetX - Settings.CurrentTileWidth
				* Settings.CurrentScale));
		int endX = (int) ((screenOffsetX + getParent().getWidth() + Settings.CurrentTileWidth
				* Settings.CurrentScale));

		if (s.getPos().y < startY || s.getPos().y > endY
				|| s.getPos().x < startX || s.getPos().x > endX)
			return;

		if (scaledSpriteImages.size() == spriteSets.get(spriteset).getSprites()
				.size()) {
			if (scaledSpriteImages.get(s.getVal()) != null)
				bi = scaledSpriteImages.get(s.getVal());
		} else
			bi = spriteSets.get(spriteset).getSprites().get(s.getVal());

		if (Math.abs(Settings.CurrentScale - previousSpriteScaleFactor) > 0.01f) {
			previousSpriteScaleFactor = Settings.CurrentScale;
			scaledSpriteImages.clear();
			for (BufferedImage img : spriteSets.get(spriteset).getSprites()) {
				BufferedImage scaledImg = new BufferedImage(
						(int) (Settings.CurrentScale * img.getWidth()),
						(int) (Settings.CurrentScale * img.getHeight()),
						img.getType());
				AffineTransform tx = new AffineTransform();
				tx.scale(Settings.CurrentScale, Settings.CurrentScale);
				AffineTransformOp op = new AffineTransformOp(tx,
						AffineTransformOp.TYPE_BILINEAR);
				op.filter(img, scaledImg);
				scaledSpriteImages.add(scaledImg);
			}
		}
		if (isBackDarkened() && !front) {
			g2d.setColor(Color.BLACK);
			RescaleOp op = new RescaleOp(.5f, 0, null);
			bi = op.filter(bi, null);
		}
		if (isBackDarkened() && !front) {
			g2d.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, 0.2f));
		}
		int w = bi.getWidth();
		int h = bi.getHeight();
		int x = (int) ((s.getPos().x) * Settings.CurrentScale - bi.getWidth() / 2);
		int y = (int) ((s.getPos().y) * Settings.CurrentScale - bi.getHeight() / 2);

		if ((int) ((s.getPos().x) * Settings.CurrentScale) + bi.getWidth() / 2 > Settings.CurrentMapWidth
				* Settings.CurrentTileWidth * Settings.CurrentScale)
			w = (int) (Settings.CurrentMapWidth * Settings.CurrentTileWidth * Settings.CurrentScale)
					- (int) ((s.getPos().x) * Settings.CurrentScale - bi
							.getWidth() / 2);
		if (w > bi.getWidth())
			w = bi.getWidth();

		if ((int) ((s.getPos().y) * Settings.CurrentScale) + bi.getHeight() / 2 > Settings.CurrentMapHeight
				* Settings.CurrentTileHeight * Settings.CurrentScale)
			h = (int) (Settings.CurrentMapHeight * Settings.CurrentTileHeight * Settings.CurrentScale)
					- (int) ((s.getPos().y) * Settings.CurrentScale - bi
							.getHeight() / 2);

		if (h > bi.getHeight())
			h = bi.getHeight();

		if (selected) {
			if (backXor)
				g2d.setColor(new Color((background.getRGB() ^ sprite.getRGB())
						^ Color.WHITE.getRGB()));
			else
				g2d.setColor(sprite);
			g2d.fillRect(x - 5, y - 5, w + 10, h + 10);
		}

		if (w >= 0 && h >= 0 && w <= bi.getWidth() && h <= bi.getHeight())
			g2d.drawImage(bi.getSubimage(0, 0, w, h), null,
					(int) ((s.getPos().x) * Settings.CurrentScale - bi
							.getWidth() / 2), (int) ((s.getPos().y)
							* Settings.CurrentScale - bi.getHeight() / 2));
	}

	/**
	 * Used to draw a single Sprite layer.
	 * 
	 * @param g2d
	 *            - Graphics object.
	 * @param layer
	 *            - {@Link SpriteLayer}
	 * @param front
	 *            - true if the layer is currently selected.
	 */
	private void drawSpriteLayer(Graphics2D g2d, SpriteLayer layer,
			boolean front) {

		for (Sprite s : layer.getSprites()) {
			if (layer.getSprites().indexOf(s) == spriteNum)
				continue;
			else
				drawSprite(g2d, s, layer.getSpriteSetId(), false, front);
		}
		if (spriteNum < layer.getSprites().size() && spriteNum >= 0)
			drawSprite(g2d, layer.getSprite(spriteNum), layer.getSpriteSetId(),
					true, front);
	}

	private void drawPolygon(Graphics2D g2d, Vector<Point> poly,
			boolean selected, boolean front) {

		if (poly.size() < 2)
			return;

		int[] x = new int[poly.size()];
		int[] y = new int[poly.size()];
		for (int i = 0; i < poly.size(); i++) {
			x[i] = (int) (Settings.CurrentScale * poly.get(i).x);
			y[i] = (int) (Settings.CurrentScale * poly.get(i).y);
		}
		Composite normal = g2d.getComposite();
		if (backXor)
			g2d.setColor(new Color((background.getRGB() ^ polygon.getRGB())
					^ Color.WHITE.getRGB()));
		else
			g2d.setColor(polygon);

		if (isBackDarkened() && !front) {
			g2d.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, 0.4f));
		}
		Stroke s = new BasicStroke(Settings.PolygonLineThickness);
		if (selected && front) {

			s = new BasicStroke(Settings.PolygonLineThickness
					+ Settings.PolygonLineThickness);
			Color c = g2d.getColor();
			g2d.setColor(c);
			g2d.setStroke(s);
			g2d.drawPolyline(x, y, poly.size());
			s = new BasicStroke(Settings.PolygonLineThickness);

			Color b = new Color((int) (g2d.getColor().getRGB() * 0.3));
			g2d.setColor(b);
			g2d.setStroke(s);
			g2d.drawPolyline(x, y, poly.size());
			if(pointNum < x.length && pointNum >= 0)
			{
				g2d.setColor(new Color((background.getRGB() ^ polygon.getRGB()) ^ Color.WHITE.getRGB()));
				g2d.drawOval(x[pointNum]-3, y[pointNum]-3, 6, 6);
			}
			g2d.setComposite(normal);
		} else {
			g2d.setStroke(s);
			g2d.drawPolyline(x, y, poly.size());
			g2d.setComposite(normal);
		}

	}

	/**
	 * Used to draw a single Polygon layer.
	 * 
	 * @param g2d
	 *            - Graphics object.
	 * @param layer
	 *            - {@Link PolygonLayer}
	 * @param front
	 *            - true if the layer is currently selected.
	 */
	private void drawPolyLayer(Graphics2D g2d, PolygonLayer layer, boolean front) {
		for (Vector<Point> poly : layer.getPolygons()) {
			if (layer.getPolygons().indexOf(poly) == polyNum)
				continue;
			else
				drawPolygon(g2d, poly, false, front);
		}
		if (polyNum < layer.getPolygons().size() && polyNum >= 0)
			drawPolygon(g2d, layer.getPolygon(polyNum), true, front);
	}

	private void drawSingleTile(Graphics2D g2d, String tileset, int tex, int x,
			int y, boolean front) {

		if (x >= 0 && y >= 0 && x < Settings.CurrentMapWidth
				&& y < Settings.CurrentMapHeight)
			if (tex > 0) {
				if (scaledTileImages.size() == tileSets.get(tileset)
						.getTileSet().size()) {
					if (scaledTileImages.get(tex) != null)
						bi = scaledTileImages.get(tex);
				} else
					bi = tileSets.get(tileset).getTileSet().get(tex);

				if (Math.abs(Settings.CurrentScale - previousTileScaleFactor) > 0.01f) {
					previousTileScaleFactor = Settings.CurrentScale;
					scaledTileImages.clear();
					for (BufferedImage img : tileSets.get(tileset).getTileSet()) {
						BufferedImage scaledImg = new BufferedImage(
								(int) (Settings.CurrentScale * img.getWidth()),
								(int) (Settings.CurrentScale * img.getHeight()),
								img.getType());
						AffineTransform tx = new AffineTransform();
						tx.scale(Settings.CurrentScale, Settings.CurrentScale);
						AffineTransformOp op = new AffineTransformOp(tx,
								AffineTransformOp.TYPE_BILINEAR);
						op.filter(img, scaledImg);
						scaledTileImages.add(scaledImg);
					}
				}
				g2d.drawImage(
						bi,
						null,
						(int) (x * Settings.CurrentTileWidth * Settings.CurrentScale),
						(int) (y * Settings.CurrentTileHeight * Settings.CurrentScale));
				if (isBackDarkened() && !front) {
					Composite normal = g2d.getComposite();
					g2d.setComposite(AlphaComposite.getInstance(
							AlphaComposite.SRC_OVER, 0.6f));
					g2d.setColor(Color.BLACK);
					g2d.fillRect(
							(int) (x * Settings.CurrentTileWidth * Settings.CurrentScale),
							(int) (y * Settings.CurrentTileHeight * Settings.CurrentScale),
							(int) (Settings.CurrentTileWidth * Settings.CurrentScale),
							(int) (Settings.CurrentTileHeight * Settings.CurrentScale));
					g2d.setComposite(normal);
				}
			}
	}

	/**
	 * Used to draw a single Tiled layer.
	 * 
	 * @param g2d
	 *            - Graphics object.
	 * @param layer
	 *            - the texture information.
	 * @param front
	 *            - true if the layer is currently selected.
	 */
	private void drawTiledLayer(Graphics2D g2d, TiledLayer layer, boolean front) {
		int startY = (int) ((screenOffsetY - Settings.CurrentTileHeight
				* Settings.CurrentScale) / (Settings.CurrentTileHeight * Settings.CurrentScale));
		int endY = (int) ((screenOffsetY + getParent().getHeight() + Settings.CurrentTileHeight
				* Settings.CurrentScale) / (Settings.CurrentTileHeight * Settings.CurrentScale));
		int startX = (int) ((screenOffsetX - Settings.CurrentTileWidth
				* Settings.CurrentScale) / (Settings.CurrentTileWidth * Settings.CurrentScale));
		int endX = (int) ((screenOffsetX + getParent().getWidth() + Settings.CurrentTileWidth
				* Settings.CurrentScale) / (Settings.CurrentTileWidth * Settings.CurrentScale));

		for (int y = startY; y < endY; y++) {
			if (y < 0 || y >= layer.getLayer().length)
				continue;

			for (int x = startX; x < endX; x++) {
				if (x < 0 || x >= layer.getLayer()[0].length)
					continue;

				int tex = layer.getLayer()[y][x];
				drawSingleTile(g2d, layer.getTileSetId(), tex, x, y, front);
			}
		}
	}
}
