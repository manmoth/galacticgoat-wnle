package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JFileChooser;

public class ScriptInterpreter {
	/**
	 * Script specification:
	 * 
	 * Available metadata: - "%W" The width in tiles of the map. - "%TW" The
	 * width in pixels of a single tile. - "%H" The height in tiles of the map.
	 * - "%TH" The height in pixels of a single tile. - "%C" The total number of
	 * layers in the map.
	 * 
	 * Commands:
	 * 
	 * Export ExcludeEmpty
	 * 
	 * Element #Name: #Start: #Data: #End: #Attr: EndElement
	 * 
	 * 
	 * LayerTile #Name: #Start: #Data: #End: #Attr:
	 * 
	 * Available data in all # fields: -"%L" - The layer index. Available data
	 * in the "Body" field: -"%X" - The X index of the tile. -"%Y" - The Y index
	 * of the tile. -"%T" - The texture value of the tile.
	 * 
	 * 
	 * RowTile #Name: #Start: #Data: #End: #Attr:
	 * 
	 * Available data in all # fields: -"%R" - The row number of the tile.
	 * Available data in the "Data" field: -"%X" - The X index of the tile.
	 * -"%Y" - The Y index of the tile. -"%T" - The texture value of the tile.
	 * 
	 * 
	 * SingleTile #Name: #Start: #Data: #End: #Attr:
	 * 
	 * Available data in all # fields: -"%R" - The row number of the tile.
	 * Available data in the "Data" field: -"%X" - The X index of the tile.
	 * -"%Y" - The Y index of the tile. -"%T" - The texture value of the tile.
	 * 
	 * 
	 * LayerPoly #Name: #Start: #Data: #End: #Attr:
	 * 
	 * Available data in the "Data" part: -"%X" - The X position of the point.
	 * -"%Y" - The Y position of the point. -"%L" - The layer index.
	 * 
	 * 
	 * SinglePoly #Name: #Start: #Data: #End: #Attr:
	 * 
	 * Available data in the "Data" part: -"%X" - The X position of the point.
	 * -"%Y" - The Y position of the point. -"%L" - The point index.
	 * 
	 * LayerSprite #Name: #Start: #Data: #End: #Attr:
	 * 
	 * Available data in the "Data" part: -"%X" - The X position of the sprite.
	 * -"%Y" - The Y position of the sprite. -"%T" - The texture value of the
	 * sprite. -"%L" - The layer index.
	 * 
	 * SingleSprite #Name: #Start: #Data: #End: #Attr:
	 * 
	 * Available data in the "Data" part: -"%X" - The X position of the sprite.
	 * -"%Y" - The Y position of the sprite. -"%T" - The texture value of the
	 * sprite. -"%L" - The layer index.
	 * 
	 */

	/**
	 * Constructor. Will create the folders scripts, scripts/export and
	 * scripts/import if they are not already present in the working folder.
	 */
	public ScriptInterpreter() {
		File dir = new File("scripts");
		if (!dir.exists())
			dir.mkdir();
		dir = new File("scripts" + File.separator + "export");
		if (!dir.exists())
			dir.mkdir();
		dir = new File("scripts" + File.separator + "import");
		if (!dir.exists())
			dir.mkdir();
	}

	/**
	 * Use this method to get the export scripts available for use.
	 * 
	 * @return {@link File}[] of all the available export scripts.
	 */
	public static File[] getAvailableExportScripts() {

		File dir = new File("scripts" + File.separator + "export"
				+ File.separator);
		File[] files = dir.listFiles();
		if (files == null)
			files = new File[1];
		return files;
	}

	/**
	 * Use this method to get the parsed text of a script.
	 * 
	 * @param file
	 *            - The File of the script to be returned.
	 * @return Vector<Vector<String>> containing the instructions of the script.
	 */
	public static Vector<Vector<String>> getScript(File file) {
		Lexer lexer = new Lexer();
		return lexer.parseFile(file);
	}

	/**
	 * Use this method to get the import scripts available for use.
	 * 
	 * @return {@link File}[] of all the available import scripts.
	 */
	public static File[] getAvailableImportScripts() {

		File dir = new File("scripts" + File.separator + "import");
		File[] files = dir.listFiles();
		if (files == null)
			files = new File[0];
		return files;
	}

	/**
	 * Use this method to add scripts to script folder.
	 * 
	 * @param scriptName
	 *            - The name to use for the script.
	 */
	public void addNewScript(String scriptName) {

		Lexer lexer = new Lexer();

		JFileChooser fch = new JFileChooser();

		String folder;

		if (fch.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
			return;
		File f;
		f = new File(fch.getSelectedFile().getAbsolutePath());

		Vector<Vector<String>> script = lexer.parseFile(f);

		if (script.get(0).get(0).equals("EXPORT"))
			folder = "scripts" + File.separator + "export" + File.separator;
		else if (script.get(0).get(0).equals("IMPORT"))
			folder = "scripts" + File.separator + "export" + File.separator;
		else
			return;

		try {
			File s = new File(folder + scriptName + ".wnlescript");

			FileWriter fw;
			if (s.getAbsolutePath().endsWith(".txt"))
				fw = new FileWriter(s);
			else
				fw = new FileWriter(s + ".txt");

			BufferedReader input = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = input.readLine()) != null) {
				fw.write(line);
				fw.write(System.getProperty("line.separator"));
			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

/**
 * The Lexer class is used to parse text files and return any keywords that are
 * in the script specification. It does not check for semantics however.
 */
class Lexer {
	/**
	 * This {@link HashMap} is used to find out if a word in the text file
	 * belongs in the script.
	 */
	Map<String, String> hm = new HashMap<String, String>(16);

	/**
	 * Constructor. Adds all the keywords to the hashmap.
	 */
	public Lexer() {
		hm.put("EXCLUDEEMPTY", "EXCLUDEEMPTY");
		hm.put("NOBREAK", "NOBREAK");
		hm.put("IMPORT", "IMPORT");
		hm.put("EXPORT", "EXPORT");
		hm.put("ELEMENT", "ELEMENT");
		hm.put("END", "TAGEND");
		hm.put("#NAME:", "NAME");
		hm.put("#START:", "START");
		hm.put("#DATA:", "DATA");
		hm.put("#END:", "END");
		hm.put("#ATTR:", "ATTR");
		hm.put("LAYERPOLY", "LAYERPOLY");
		hm.put("SINGLEPOLY", "SINGLEPOLY");
		hm.put("POINTPOLY", "POINTPOLY");
		hm.put("LAYERTILE", "LAYERTILE");
		hm.put("ROWTILE", "ROWTILE");
		hm.put("SINGLETILE", "SINGLETILE");
		hm.put("LAYERSPRITE", "LAYERSPRITE");
		hm.put("SINGLESPRITE", "SINGLESPRITE");
	}

	/**
	 * 
	 * @param file
	 *            - {@link File} to be parsed.
	 * @return - Vector<Vector<String>> containing the parsed text as strings.
	 */
	public Vector<Vector<String>> parseFile(File file) {

		Vector<Vector<String>> script = new Vector<Vector<String>>();

		try {
			BufferedReader input = new BufferedReader(new FileReader(file));
			try {
				String line = null;
				while ((line = input.readLine()) != null) {
					line = line.toUpperCase();
					line.trim();

					Vector<String> instruction = new Vector<String>();

					StringBuilder temp = new StringBuilder();
					boolean literal = false;

					for (char c : line.toCharArray()) {

						if (!literal && c == ' ')
							continue;
						else if (literal && c == '#') {
							literal = false;
							instruction.add(temp.toString());
							temp = new StringBuilder();
						}

						temp.append(c);

						if (!literal) {
							String s = hm.get(temp.toString());
							if (s != null) {
								instruction.add(s);
								if (temp.charAt(temp.length() - 1) == ':')
									literal = true;
								temp = new StringBuilder();
							}
						}
					}
					if (literal)
						instruction.add(temp.toString());
					if (instruction.size() > 0)
						script.add(instruction);

				}
			} finally {
				input.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return script;

	}
}
