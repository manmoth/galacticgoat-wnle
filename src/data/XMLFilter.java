package data;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * FileFilter that allows only xml files.
 */
public class XMLFilter extends FileFilter {

	public boolean accept(File file) {
		String ext = file.getName();

		if (file.isDirectory())
			return true;

		else if (ext != null && (ext.endsWith(".xml")))
			return true;

		else
			return false;
	}

	public String getDescription() {
		return ".xml";
	}
}
