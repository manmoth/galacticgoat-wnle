package data;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

import view.Settings;

/**
 * Contains static classes that are used to load an image and return an
 * image-vector with tile-sized images.
 */
public class ImageLoader {

	/**
	 * Method used to cut larger images into tiles.
	 * 
	 * @param name
	 *            - the name of the tileset.
	 * @param files
	 *            - the files of the images.
	 * 
	 * @return An array of tile-sized {@link BufferedImage} cut from the
	 *         selected image, returns null if no image is selected.
	 */
	public static TileSet loadTileset(String name, Vector<File> files) {

		if (files.size() == 0) {
			JFileChooser fch = new JFileChooser();
			fch.setCurrentDirectory(currentDirectory);
			fch.setFileFilter(new ImageFilter());
			fch.setMultiSelectionEnabled(false);
			fch.setAcceptAllFileFilterUsed(false);
			fch.setDialogTitle("Select tileset image");
			int returnVal = fch.showOpenDialog(null);
			currentDirectory = fch.getCurrentDirectory();
			files.add(fch.getSelectedFile());
			if (returnVal == JFileChooser.CANCEL_OPTION)
				return null;
		}
		Vector<BufferedImage> img = loadImages(files);

		if (img == null || img.size() == 0)
			return null;

		BufferedImage tileSet = img.firstElement();
		if (tileSet == null)
			return null;
		else {
			int numHorizontal = tileSet.getWidth() / Settings.CurrentTileWidth;
			int numVertical = tileSet.getHeight() / Settings.CurrentTileHeight;

			Vector<BufferedImage> tiles = new Vector<BufferedImage>();
			GraphicsEnvironment env = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			GraphicsDevice device = env.getDefaultScreenDevice();
			GraphicsConfiguration config = device.getDefaultConfiguration();
			tiles.add(config.createCompatibleImage(Settings.CurrentTileWidth,
					Settings.CurrentTileHeight, Transparency.TRANSLUCENT));

			for (int y = 0; y < numVertical; y++) {
				for (int x = 0; x < numHorizontal; x++) {
					BufferedImage bi = config.createCompatibleImage(
							Settings.CurrentTileWidth,
							Settings.CurrentTileHeight,
							Transparency.TRANSLUCENT);
					bi.createGraphics().drawImage(
							tileSet.getSubimage(x * Settings.CurrentTileWidth,
									y * Settings.CurrentTileHeight,
									Settings.CurrentTileWidth,
									Settings.CurrentTileHeight), null, 0, 0);

					tiles.add(bi);

				}
			}
			return new TileSet(tiles, files, name);
		}
	}

	public static SpriteSet loadSpriteSet(String name, Vector<File> files) {

		if (files.size() == 0) {
			JFileChooser fch = new JFileChooser();
			fch.setCurrentDirectory(currentDirectory);
			fch.setFileFilter(new ImageFilter());
			fch.setMultiSelectionEnabled(true);
			fch.setAcceptAllFileFilterUsed(false);
			fch.setDialogTitle("Select images to use as sprites");
			int returnVal = fch.showOpenDialog(null);
			currentDirectory = fch.getCurrentDirectory();
			if (returnVal == JFileChooser.CANCEL_OPTION)
				return null;
			for (File f : fch.getSelectedFiles())
				files.add(f);
		}
		return new SpriteSet(loadImages(files), files, name);
	}

	private static File currentDirectory = null;

	public static void SetWorkingDirectory(String path) {
		currentDirectory = new File(path);
	}

	/**
	 * Method used to import image files.
	 * 
	 * @return The selected file as a {@link BufferedImage} or null if no file
	 *         was selected.
	 */
	private static Vector<BufferedImage> loadImages(Vector<File> files) {
		Vector<BufferedImage> img = new Vector<BufferedImage>(files.size());
		for (File f : files) {
			try {
				BufferedImage b = ImageIO.read(f);
				img.add(b);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return img;
	}
}
