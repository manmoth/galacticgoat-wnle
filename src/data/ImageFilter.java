package data;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * FileFilter that allows only png, jpg, jpeg and bmp files.
 */
public class ImageFilter extends FileFilter {

	public boolean accept(File file) {
		String ext = file.getName();

		if (file.isDirectory())
			return true;

		else if (ext != null
				&& (ext.endsWith(".png") || ext.endsWith(".jpg")
						|| ext.endsWith(".jpeg") || ext.endsWith(".bmp")))
			return true;

		else
			return false;
	}

	public String getDescription() {
		return ".png, .jpg, .bmp and .jpeg Files";
	}
}
