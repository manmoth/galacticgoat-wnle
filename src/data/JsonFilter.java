package data;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class JsonFilter extends FileFilter {

	public boolean accept(File file) {
		String ext = file.getName();

		if (file.isDirectory())
			return true;

		else if (ext != null && (ext.endsWith(".json")))
			return true;

		else
			return false;
	}

	public String getDescription() {
		return ".json";
	}
}
