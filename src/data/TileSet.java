package data;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Vector;

public class TileSet {
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private Vector<File> files;

	public Vector<File> getFiles() {
		return files;
	}

	public void setFiles(Vector<File> files) {
		this.files = files;
	}

	private Vector<BufferedImage> tileSet;

	public Vector<BufferedImage> getTileSet() {
		return tileSet;
	}

	public TileSet(Vector<BufferedImage> tileSet, Vector<File> files, String id) {
		this.id = id;
		this.files = files;
		this.tileSet = tileSet;
	}

}
