package data;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Vector;

public class SpriteSet {
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private Vector<File> files;

	public Vector<File> getFiles() {
		return files;
	}

	public void setFiles(Vector<File> files) {
		this.files = files;
	}

	private Vector<BufferedImage> sprites;

	public Vector<BufferedImage> getSprites() {
		return sprites;
	}

	public void setSprites(Vector<BufferedImage> sprites) {
		this.sprites = sprites;
	}

	public SpriteSet(Vector<BufferedImage> sprites, Vector<File> files,
			String id) {
		this.id = id;
		this.files = files;
		this.sprites = sprites;
	}
}
