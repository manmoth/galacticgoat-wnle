package data;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import logic.HybridMap;
import logic.Layer;
import logic.MapInterface;
import logic.MapManager.MapType;
import logic.PolygonLayer;
import logic.Sprite;
import logic.SpriteLayer;
import logic.TiledLayer;
import logic.TiledMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import view.Settings;

public class MapExporter {

	public enum IOType {
		TXT, JSON, XML, PNG, USER
	}

	private static File currentDirectory = null;

	public static void SetWorkingDirectory(String path) {
		currentDirectory = new File(path);
	}

	/**
	 * Method used to parse a literal and replace all known expressions with
	 * their map-specific value. For example will %X be replaced with the actual
	 * X value of the tile that is being written.
	 * 
	 * @param literal
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param tileh
	 * @param tilew
	 * @param index
	 * @param tex
	 * @param total
	 * @param name
	 * @return
	 */
	private static String parseLiteral(String literal, int x, int y, int w,
			int h, int tileh, int tilew, int index, int tex, int total) {
		if (literal.contains("%X"))
			literal = literal.replaceAll("%X", String.valueOf(x));
		if (literal.contains("%Y"))
			literal = literal.replaceAll("%Y", String.valueOf(y));
		if (literal.contains("%W"))
			literal = literal.replaceAll("%W", String.valueOf(w));
		if (literal.contains("%H"))
			literal = literal.replaceAll("%H", String.valueOf(h));
		if (literal.contains("%TH"))
			literal = literal.replaceAll("%TH", String.valueOf(tileh));
		if (literal.contains("%TW"))
			literal = literal.replaceAll("%TW", String.valueOf(tilew));
		if (literal.contains("%T"))
			literal = literal.replaceAll("%T", String.valueOf(tex));
		if (literal.contains("%L"))
			literal = literal.replaceAll("%L", String.valueOf(index));
		if (literal.contains("%R"))
			literal = literal.replaceAll("%R", String.valueOf(index));
		if (literal.contains("%C"))
			literal = literal.replaceAll("%C", String.valueOf(total));
		literal = literal.trim();
		return literal;
	}

	private static String parseLiteral(String literal, int x, int y, int w,
			int h, int tileh, int tilew, int index, int tex, int total,
			String name) {
		literal = parseLiteral(literal, x, y, w, h, tileh, tilew, index, tex,
				total);
		if (literal.contains("%N"))
			literal = literal.replaceAll("%N", name);
		literal = literal.trim();
		return literal;
	}

	/**
	 * This method will convert keywords from the vector instruction and return
	 * a string with the text this instruction translates into.
	 * 
	 * @param tagStack
	 *            - Used to save text that is used both to start and end tags.
	 * @param instruction
	 *            - Contains the instructions that will be translated.
	 * @param map
	 *            - The current {@link MapInterface}.
	 * @return
	 */
	private static String getLineFromInstruction(Vector<String> tagStack,
			Vector<String> instruction, MapInterface map, boolean excludeEmpty,
			boolean nobreak) {

		TiledMap tm = (TiledMap) map;

		int w = Settings.CurrentMapWidth;
		int h = Settings.CurrentMapHeight;
		int tw = Settings.CurrentTileWidth;
		int th = Settings.CurrentTileHeight;

		StringBuilder line = new StringBuilder();

		for (int i = 0; i < instruction.size(); i++) {
			if (instruction.get(i).equals("ELEMENT")) {
				line.append("<");
			} else if (instruction.get(i).equals("NAME")) {
				Vector<String> attributes = new Vector<String>();
				for (int s = 0; s < instruction.size(); s++)
					if (instruction.get(s).equals("ATTR"))
						attributes.add(instruction.get(s + 1));

				line.append(parseLiteral(instruction.get(++i), 0, 0, w, h, tw,
						th, 0, 0, tm.getLayers().size()));

				for (String s : attributes)
					line.append(" "
							+ parseLiteral(s, 0, 0, w, h, tw, th, 0, 0, tm
									.getLayers().size()));

				line.append(">");
				if (!nobreak)
					line.append(System.getProperty("line.separator"));
				tagStack.add(instruction.get(i));
			} else if (instruction.get(i).equals("START")) {
				line.append(instruction.get(++i));
			} else if (instruction.get(i).equals("DATA")) {
				line.append(parseLiteral(instruction.get(++i), 0, 0, w, h, tw,
						th, 0, 0, tm.getTileLayers().size()));
			} else if (instruction.get(i).equals("END")) {
				// tagStack.add(instruction.get(++i));
			} else if (instruction.get(i).equals("TAGEND")) {
				line.append("</"
						+ parseLiteral(tagStack.lastElement(), 0, 0, w, h, tw,
								th, 0, 0, tm.getTileLayers().size()) + ">");
				tagStack.remove(tagStack.size() - 1);
			} else if (tm instanceof HybridMap) {
				if (instruction.get(i).equals("LAYERPOLY")
						|| instruction.get(i).equals("SINGLEPOLY")
						|| instruction.get(i).equals("POINTPOLY")) {

					String elementType = instruction.get(i);
					StringBuilder name = new StringBuilder();
					String startText = "";
					String endText = "";
					while (i < instruction.size()) {
						if (instruction.get(i).equals("NAME")) {
							Vector<String> attributes = new Vector<String>();

							for (int s = 0; s < instruction.size(); s++)
								if (instruction.get(s).equals("ATTR"))
									attributes.add(instruction.get(s + 1));
							for (String s : attributes)
								name.append(" " + s);
							if (!nobreak)
								line.append(System
										.getProperty("line.separator"));
							tagStack.add(instruction.get(++i));
						} else if (instruction.get(i).equals("START")) {
							startText = instruction.get(++i);
						} else if (instruction.get(i).equals("END")) {
							endText = instruction.get(++i);
						} else if (instruction.get(i).equals("DATA")) {
							i++;
							for (PolygonLayer layer : ((HybridMap) map)
									.getPolyLayers()) {

								if (elementType.equals("LAYERPOLY"))
									line.append("<"
											+ parseLiteral(
													tagStack.lastElement()
															+ name.toString(),
													0, 0, w, h, tw, th,
													((HybridMap) map)
															.getPolyLayers()
															.indexOf(layer), 0,
													((HybridMap) map)
															.getPolyLayers()
															.size()) + ">"
											+ startText);
								if (!nobreak)
									line.append(System
											.getProperty("line.separator"));

								for (Vector<Point> polygon : layer
										.getPolygons()) {
									if (polygon.size() == 0 && excludeEmpty)
										continue;

									if (elementType.equals("SINGLEPOLY")
											|| elementType.equals("LAYERPOLY"))
										line.append("<"
												+ parseLiteral(
														tagStack.lastElement()
																+ name.toString(),
														0, 0, w, h, tw, th,
														layer.indexOf(polygon),
														0, layer.size()) + ">"
												+ startText);
									for (int p = 0; p < polygon.size(); p++) {
										if (elementType.equals("POINTPOLY"))
											line.append("<"
													+ parseLiteral(
															tagStack.lastElement()
																	+ name.toString(),
															0, 0, w, h, tw, th,
															p, 0, polygon
																	.size())
													+ ">" + startText);

										line.append(parseLiteral(
												instruction.get(i),
												polygon.get(p).x,
												polygon.get(p).y, 0, 0, 0, 0,
												p, 0, polygon.size()));
										if (elementType.equals("POINTPOLY"))
											line.append(endText
													+ "</"
													+ parseLiteral(tagStack
															.lastElement(), 0,
															0, w, h, tw, th, p,
															0, polygon.size())
													+ ">");
									}
									if (elementType.equals("SINGLEPOLY")
											|| elementType.equals("LAYERPOLY"))
										line.append(endText
												+ "</"
												+ parseLiteral(
														tagStack.lastElement(),
														0, 0, w, h, tw, th,
														layer.indexOf(polygon),
														0, layer.size()) + ">");
									if (!nobreak)
										line.append(System
												.getProperty("line.separator"));
								}

								if (elementType.equals("LAYERPOLY"))
									line.append(endText
											+ "</"
											+ parseLiteral(tagStack
													.lastElement(), 0, 0, w, h,
													tw, th, ((HybridMap) map)
															.getPolyLayers()
															.indexOf(layer), 0,
													((HybridMap) map)
															.getPolyLayers()
															.size()) + ">");
								if (!nobreak)
									line.append(System
											.getProperty("line.separator"));
							}
							tagStack.remove(tagStack.size() - 1);
						} else if (instruction.get(i).equals("END")) {
							// tagStack.add(instruction.get(++i));
							if (!nobreak)
								line.append(System
										.getProperty("line.separator"));
						}
						i++;
					}
				} else if (instruction.get(i).equals("LAYERSPRITE")
						|| instruction.get(i).equals("SINGLESPRITE")) {

					String elementType = instruction.get(i);
					StringBuilder name = new StringBuilder();
					String startText = "";
					String endText = "";
					while (i < instruction.size()) {
						if (instruction.get(i).equals("NAME")) {
							Vector<String> attributes = new Vector<String>();

							for (int s = 0; s < instruction.size(); s++)
								if (instruction.get(s).equals("ATTR"))
									attributes.add(instruction.get(s + 1));
							for (String s : attributes)
								name.append(" " + s);
							if (!nobreak)
								line.append(System
										.getProperty("line.separator"));
							tagStack.add(instruction.get(++i));
						} else if (instruction.get(i).equals("START")) {
							startText = instruction.get(++i);
						} else if (instruction.get(i).equals("END")) {
							endText = instruction.get(++i);
						} else if (instruction.get(i).equals("DATA")) {
							i++;
							for (SpriteLayer layer : ((HybridMap) map)
									.getSpriteLayers()) {
								line.append("<"
										+ parseLiteral(tagStack.lastElement(),
												0, 0, w, h, tw, th,
												((HybridMap) map)
														.getPolyLayers()
														.indexOf(layer), 0,
												((HybridMap) map)
														.getPolyLayers().size())
										+ ">" + startText);
								if (!nobreak)
									line.append(System
											.getProperty("line.separator"));

								for (Sprite sprite : layer.getSprites()) {
									int tex = sprite.getVal();
									String n = sprite.getName();

									if (elementType.equals("SINGLESPRITE"))
										line.append("<"
												+ parseLiteral(
														tagStack.lastElement()
																+ name.toString(),
														sprite.getPos().x,
														sprite.getPos().y,
														w,
														h,
														tw,
														th,
														layer.getSprites()
																.indexOf(sprite),
														tex, layer.getSprites()
																.size()) + ">"
												+ startText);

									line.append(parseLiteral(
											instruction.get(i),
											sprite.getPos().x,
											sprite.getPos().y, w, h, tw, th,
											layer.getSprites().indexOf(sprite),
											tex, tm.getTileLayers().size(), n));

									if (elementType.equals("SINGLESPRITE"))
										line.append(endText
												+ "</"
												+ parseLiteral(
														tagStack.lastElement(),
														sprite.getPos().x,
														sprite.getPos().y,
														w,
														h,
														tw,
														th,
														layer.getSprites()
																.indexOf(sprite),
														tex, layer.getSprites()
																.size()) + ">");
									if (!nobreak)
										line.append(System
												.getProperty("line.separator"));
								}
								line.append(endText
										+ "</"
										+ parseLiteral(tagStack.lastElement(),
												0, 0, w, h, tw, th,
												((HybridMap) map)
														.getPolyLayers()
														.indexOf(layer), 0,
												((HybridMap) map)
														.getPolyLayers().size())
										+ ">");
								if (!nobreak)
									line.append(System
											.getProperty("line.separator"));
							}
							tagStack.remove(tagStack.size() - 1);
						} else if (instruction.get(i).equals("END")) {
							// tagStack.add(instruction.get(++i));
							if (!nobreak)
								line.append(System
										.getProperty("line.separator"));
						}
						i++;
					}
				} else if (instruction.get(i).equals("LAYERTILE")
						|| instruction.get(i).equals("ROWTILE")
						|| instruction.get(i).equals("SINGLETILE")) {

					String elementType = instruction.get(i);

					while (i < instruction.size()) {
						if (instruction.get(i).equals("NAME")) {
							tagStack.add(instruction.get(++i));
						} else if (instruction.get(i).equals("START")) {
							line.append(instruction.get(++i));
						} else if (instruction.get(i).equals("DATA")) {
							i++;
							for (TiledLayer layer : tm.getTileLayers()) {
								line.append("<"
										+ parseLiteral(tagStack.lastElement(),
												0, 0, w, h, tw, th, tm
														.getTileLayers()
														.indexOf(layer), 0, tm
														.getTileLayers().size())
										+ ">");

								for (int y = 0; y < Settings.CurrentMapHeight; y++) {
									if (elementType.equals("ROWTILE"))
										line.append("<"
												+ parseLiteral(tagStack
														.lastElement(), 0, 0,
														w, h, tw, th, y, 0,
														tm.getTileLayers()
																.size()) + ">");
									for (int x = 0; x < Settings.CurrentMapWidth; x++) {
										int tex = layer.getLayer()[y][x];
										if (tex == 0 && excludeEmpty)
											continue;

										if (elementType.equals("SINGLETILE"))
											line.append("<"
													+ parseLiteral(
															tagStack.lastElement(),
															0,
															0,
															w,
															h,
															tw,
															th,
															x
																	+ (y * Settings.CurrentMapWidth),
															0,
															tm.getTileLayers()
																	.size())
													+ ">");

										line.append(parseLiteral(instruction
												.get(i), x, y, w, h, tw, th, x,
												tex, tm.getTileLayers().size()));
										if (elementType.equals("SINGLETILE"))
											line.append("</"
													+ parseLiteral(
															tagStack.lastElement(),
															0,
															0,
															w,
															h,
															tw,
															th,
															x
																	+ (y * Settings.CurrentMapWidth),
															0,
															tm.getTileLayers()
																	.size())
													+ ">");
									}
									if (elementType.equals("ROWTILE"))
										line.append("</"
												+ parseLiteral(tagStack
														.lastElement(), 0, 0,
														w, h, tw, th, y, 0,
														tm.getTileLayers()
																.size()) + ">");
									if (!nobreak)
										line.append(System
												.getProperty("line.separator"));
								}
								line.append("</"
										+ parseLiteral(tagStack.lastElement(),
												0, 0, w, h, tw, th, tm
														.getTileLayers()
														.indexOf(layer), 0, tm
														.getTileLayers().size())
										+ ">"
										+ System.getProperty("line.separator"));
							}
							tagStack.remove(tagStack.size() - 1);
						} else if (instruction.get(i).equals("END")) {
							// tagStack.add(instruction.get(++i));
							if (!nobreak)
								line.append(System
										.getProperty("line.separator"));
						}
						i++;
					}
				}
			}
		}
		return line.toString();
	}

	/**
	 * Use this method to export a map to a XML file on disk using a
	 * user-specified script. A {@link JFileChooser} will let the user select a
	 * specific folder and name for the output file.
	 * 
	 * @param map
	 *            - the {@link MapInterface} object to be exported.
	 * @param file
	 *            - the {@link File} of the script to be used.
	 * 
	 */
	public static void exportToXML(File file, MapInterface map) {

		JFileChooser fch = new JFileChooser();
		fch.setCurrentDirectory(currentDirectory);
		fch.setFileFilter(new XMLFilter());
		fch.setAcceptAllFileFilterUsed(false);

		FileWriter fw;
		Vector<Vector<String>> script = ScriptInterpreter.getScript(file);
		Vector<String> tagStack = new Vector<String>();

		if (fch.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			try {
				if (fch.getSelectedFile().getAbsolutePath().endsWith(".xml"))
					fw = new FileWriter(fch.getSelectedFile());
				else
					fw = new FileWriter(fch.getSelectedFile() + ".xml");

				fw.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
						+ System.getProperty("line.separator"));

				boolean excludeEmpty = false;
				boolean nobreak = false;

				for (Vector<String> instruction : script) {
					if (instruction.get(0).equals("EXCLUDEEMPTY"))
						excludeEmpty = true;
					else if (instruction.get(0).equals("NOBREAK"))
						nobreak = true;
				}

				for (Vector<String> instruction : script)
					fw.write(getLineFromInstruction(tagStack, instruction, map,
							excludeEmpty, nobreak));

				fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		currentDirectory = fch.getCurrentDirectory();
	}

	/**
	 * Use this method to export a map to a XML file on disk. A
	 * {@link JFileChooser} will let the user select a specific folder and name
	 * for the output file.
	 * 
	 * @param map
	 *            - the {@link MapInterface} object to be exported.
	 */
	public static void exportToXML(MapInterface map) {

		JFileChooser fch = new JFileChooser();
		fch.setCurrentDirectory(currentDirectory);
		fch.setFileFilter(new XMLFilter());
		fch.setAcceptAllFileFilterUsed(false);

		if (fch.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			try {

				DocumentBuilderFactory mapDoc = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = mapDoc.newDocumentBuilder();
				Document doc = docBuilder.newDocument();

				File f;
				if (fch.getSelectedFile().getAbsolutePath().endsWith(".xml"))
					f = new File(fch.getSelectedFile().getAbsolutePath());
				else
					f = new File(fch.getSelectedFile().getAbsolutePath()
							+ ".xml");

				if (map.getType() == MapType.TILED) {

					Element root = doc.createElement("map");
					doc.appendChild(root);

					Element w = doc.createElement("w");
					root.appendChild(w);
					Text width = doc.createTextNode(Integer
							.toString(Settings.CurrentMapWidth));
					w.appendChild(width);

					Element h = doc.createElement("h");
					root.appendChild(h);
					Text height = doc.createTextNode(Integer
							.toString(Settings.CurrentMapHeight));
					h.appendChild(height);

					TiledMap tm = (TiledMap) map;
					for (TiledLayer layer : tm.getTileLayers()) {
						Element lay = doc.createElement("layer");
						root.appendChild(lay);

						for (int y = 0; y < Settings.CurrentMapHeight; y++) {
							StringBuilder sb = new StringBuilder();
							for (int x = 0; x < Settings.CurrentMapWidth; x++) {
								int tex = layer.getLayer()[y][x];
								sb.append(tex + ",");
							}
							Text t = doc.createTextNode(sb.toString());
							Element l = doc.createElement("l");
							l.appendChild(t);
							lay.appendChild(l);
						}
					}
					// set up a transformer
					TransformerFactory tf = TransformerFactory.newInstance();
					Transformer t = tf.newTransformer();

					StreamResult result = new StreamResult(f);
					DOMSource source = new DOMSource(doc);
					t.transform(source, result);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		currentDirectory = fch.getCurrentDirectory();
	}

	/**
	 * Use this method to export a map to a txt file on disk. A
	 * {@link JFileChooser} will let the user select a specific folder and name
	 * for the output file.
	 * 
	 * @param map
	 *            - the {@link MapInterface} object to be exported.
	 */
	public static void exportToTXT(MapInterface map) {

		JFileChooser fch = new JFileChooser();
		fch.setFileFilter(new TextFilter());
		fch.setAcceptAllFileFilterUsed(false);

		if (fch.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			try {
				FileWriter fw;
				if (fch.getSelectedFile().getAbsolutePath().endsWith(".txt"))
					fw = new FileWriter(fch.getSelectedFile());
				else
					fw = new FileWriter(fch.getSelectedFile() + ".txt");

				if (map.getType() == MapType.TILED) {
					TiledMap tm = (TiledMap) map;
					int layerCount = 0;
					for (TiledLayer layer : tm.getTileLayers()) {
						fw.write("int " + "layer" + layerCount + "["
								+ Settings.CurrentMapWidth + "]" + "["
								+ Settings.CurrentMapHeight + "] = {");
						fw.write(System.getProperty("line.separator"));
						for (int y = 0; y < Settings.CurrentMapHeight; y++) {
							fw.write("{ ");
							for (int x = 0; x < Settings.CurrentMapWidth; x++) {
								int tex = layer.getLayer()[y][x];
								if (x == Settings.CurrentMapWidth - 1)
									fw.write(tex + " ");
								else
									fw.write(tex + ", ");
							}
							if (y == Settings.CurrentMapHeight - 1)
								fw.write("}"
										+ System.getProperty("line.separator"));
							else
								fw.write("},"
										+ System.getProperty("line.separator"));
						}
						fw.write("};" + System.getProperty("line.separator"));
						fw.write(System.getProperty("line.separator"));
						layerCount++;
					}

				}
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	/**
	 * Use this method to export a map to a json file on disk. A
	 * {@link JFileChooser} will let the user select a specific folder and name
	 * for the output file.
	 * 
	 * @param map
	 *            - the {@link MapInterface} object to be exported.
	 */
	public static void exportToJSON(MapInterface map) {

		JFileChooser fch = new JFileChooser();
		fch.setCurrentDirectory(currentDirectory);
		fch.setFileFilter(new JsonFilter());
		fch.setAcceptAllFileFilterUsed(false);

		if (fch.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			try {
				FileWriter fw;
				if (fch.getSelectedFile().getAbsolutePath().endsWith(".json"))
					fw = new FileWriter(fch.getSelectedFile());
				else
					fw = new FileWriter(fch.getSelectedFile() + ".json");

				if (map.getType() == MapType.HYBRID) {
					HybridMap tm = (HybridMap) map;
					int layerCount = 0;
					fw.write("{level:{");
					fw.write(System.getProperty("line.separator"));
					// fw.write("width:"+Settings.CurrentMapWidth+",height:"+Settings.CurrentMapHeight+",");
					fw.write("tilewidth:" + Settings.CurrentTileWidth
							+ ",tileheight:" + Settings.CurrentTileHeight + ",");
					fw.write("width:" + Settings.CurrentMapWidth
							+ ",height:" + Settings.CurrentMapHeight + ",");
					for (Object obj : tm.getLayers()) {
						Layer layer = (Layer) obj;
						fw.write(System.getProperty("line.separator"));
						fw.write(layer.getName() + ":[");
						fw.write(System.getProperty("line.separator"));

						if (layer instanceof TiledLayer) {
							TiledLayer tl = (TiledLayer) layer;
							for (int y = 0; y < Settings.CurrentMapHeight; y++) {
								for (int x = 0; x < Settings.CurrentMapWidth; x++) {
									int tex = tl.getLayer()[y][x];
									if (tex > 0) {
										fw.write("{ t:"
												+ tex
												+ ",x:"
												+ (x
														* Settings.CurrentTileWidth + Settings.CurrentTileWidth / 2)
												+ ",y:"
												+ (y
														* Settings.CurrentTileHeight + Settings.CurrentTileHeight / 2)
												+ "},");
									}
								}
							}
						}
						if (layer instanceof PolygonLayer) {
							PolygonLayer pl = (PolygonLayer) layer;
							for (Vector<Point> plg : pl.getPolygons()) {
								fw.write("[");
								for (Point p : plg) {
									fw.write("{ x:" + p.x + ",y:" + p.y + "},");
								}
								fw.write("],");
								fw.write(System.getProperty("line.separator"));
							}
						}
						if (layer instanceof SpriteLayer) {
							SpriteLayer sl = (SpriteLayer) layer;
							for (Sprite spr : sl.getSprites()) {
								fw.write("{ t:" + spr.getVal() + ",x:"
										+ spr.getPos().x + ",y:"
										+ spr.getPos().y + "},");
								fw.write(System.getProperty("line.separator"));
							}
						}
						fw.write(System.getProperty("line.separator"));
						layerCount++;
						fw.write("],");
					}

					fw.write("}}");
				}
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		currentDirectory = fch.getCurrentDirectory();
	}

	/**
	 * Use this method to export a map to a png image on disk. A JFileChooser
	 * will let you select a specific folder and name for the output file.
	 * 
	 * @param map
	 *            - the Map object to be exported.
	 * @param tileSet
	 *            - the Vector<BufferedImage> of the tiles used to draw the map.
	 */
	public static void exportToPNG(MapInterface map,
			Vector<BufferedImage> tileSet) {

		JFileChooser fch = new JFileChooser();
		fch.setFileFilter(new ImageFilter());
		fch.setAcceptAllFileFilterUsed(false);

		if (fch.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			try {
				if (map.getType() == MapType.TILED) {
					TiledMap tm = (TiledMap) map;

					int width = Settings.CurrentTileWidth
							* Settings.CurrentMapWidth;
					int height = Settings.CurrentTileHeight
							* Settings.CurrentMapHeight;

					BufferedImage img = new BufferedImage(width, height,
							BufferedImage.TYPE_INT_RGB);

					Graphics2D g2d = img.createGraphics();
					g2d.setColor(Color.DARK_GRAY);
					g2d.fillRect(Settings.CurrentMapWidth
							* Settings.CurrentTileWidth,
							Settings.CurrentMapHeight
									* Settings.CurrentTileHeight,
							Settings.CurrentTileWidth,
							Settings.CurrentTileHeight);
					for (TiledLayer layer : tm.getTileLayers()) {
						for (int y = 0; y < Settings.CurrentMapHeight; y++) {
							for (int x = 0; x < Settings.CurrentMapWidth; x++) {
								int tex = layer.getLayer()[y][x];
								if (tex > 0)
									g2d.drawImage(
											tileSet.get(layer.getLayer()[y][x]),
											null,
											x * Settings.CurrentTileWidth,
											y * Settings.CurrentTileHeight);
							}
						}
					}
					File f;

					if (fch.getSelectedFile().getAbsolutePath()
							.endsWith(".png"))
						f = new File(fch.getSelectedFile().getAbsolutePath());
					else
						f = new File(fch.getSelectedFile().getAbsolutePath()
								+ ".png");

					ImageIO.write((RenderedImage) img, "png", f);
				}
			} catch (IOException e) {
			}
	}
}
