package data;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * FileFilter that allows only txt files.
 */
public class TextFilter extends FileFilter {

	public boolean accept(File file) {
		String ext = file.getName();

		if (file.isDirectory())
			return true;

		else if (ext != null && (ext.endsWith(".txt")))
			return true;

		else
			return false;
	}

	public String getDescription() {
		return ".txt";
	}
}
