package logic;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import logic.MapManager.MapType;
import view.Settings;

/**
 * The TiledMap class implements the MapInterface interface to create a specific
 * type of Tile-based map that uses int[][] arrays to keep track of the texture
 * value of each tile.
 */
public class TiledMap implements MapInterface, Serializable {
	private static final long serialVersionUID = -644930420635916208L;

	private Map<String, Vector<File>> tileSets;

	public Map<String, Vector<File>> getTileSets() {
		return tileSets;
	}

	public void setTileSets(Map<String, Vector<File>> tileSets) {
		this.tileSets = tileSets;
	}

	protected Vector<Object> mapLayers;
	protected Vector<int[]> layerTileOffsets;

	/**
	 * @return A int[] describing the offsets within the tileset of a layer.
	 */
	public int[] getLayerTileOffsets(int layerNum) {
		return layerTileOffsets.get(layerNum);
	}

	protected MapType type = MapType.TILED;

	@Override
	public MapType getType() {
		return type;
	}

	private int currentTex = 0;

	/**
	 * @return The index of the texture-tile to be used when changing tiles.
	 */
	public int getCurrentTex() {
		return currentTex;
	}

	/**
	 * @param currentTex
	 *            The index of the texture-tile to be used when changing tiles.
	 */
	public void setCurrentTex(int currentTex) {
		this.currentTex = currentTex;
	}

	ChangeHandler changeHandler;

	/**
	 * Use to get this maps ChangeHandler
	 * 
	 * @return ChangeHandler of this map.
	 */
	public ChangeHandler getChangeHandler() {
		return changeHandler;
	}

	protected int mapWidth;

	@Override
	public int getMapWidth() {
		return mapWidth;
	}

	protected int mapHeight;

	@Override
	public int getMapHeight() {
		return mapHeight;
	}

	@Override
	public void setMapWidth(int width) {
		mapWidth = width;
	}

	@Override
	public void setMapHeight(int height) {
		mapHeight = height;
	}

	protected int tileWidth;

	public int getTileWidth() {
		return tileWidth;
	}

	protected int tileHeight;

	public int getTileHeight() {
		return tileHeight;
	}

	/**
	 * Constructor. Will instantiate the layer-vector and try to load a tileSet.
	 */
	public TiledMap(int mapWidth, int mapHeight, int tileWidth, int tileHeight) {
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;

		changeHandler = new ChangeHandler();
		mapLayers = new Vector<Object>();
		layerTileOffsets = new Vector<int[]>();
		tileSets = new HashMap<String, Vector<File>>(0);
	}

	public Object getLayer(int layerNum) {
		return mapLayers.get(layerNum);
	}

	@Override
	public void setLayer(Object layer, int index) {
		mapLayers.set(index, layer);
	}

	@Override
	public void addLayer(String name, String tileSetId) {
		mapLayers.add(new TiledLayer(name, tileSetId, Settings.CurrentMapWidth,
				Settings.CurrentMapHeight));
		int[] tileOffset = { 0, -1 };
		layerTileOffsets.add(tileOffset);
	}

	/**
	 * Add a new layer to this map with specified offsets within the tileset
	 * 
	 * @param tileSetStart
	 *            - the index of the first tile from the tileset.
	 * @param tileSetEnd
	 *            - the index of the last tile from the tileset.
	 */
	public void addOffsetLayer(String name, String tileSetId, int tileSetStart,
			int tileSetEnd) {
		mapLayers.add(new TiledLayer(name, tileSetId, Settings.CurrentMapWidth,
				Settings.CurrentMapHeight));
		int[] tileOffset = { tileSetStart, tileSetEnd };
		layerTileOffsets.add(tileOffset);
	}

	@Override
	public void swapLayers(int first, int second) {
		Object layer = mapLayers.remove(first);
		mapLayers.insertElementAt(mapLayers.elementAt(second - 1), first);
		mapLayers.insertElementAt(layer, second);
		mapLayers.remove(second + 1);

		int[] layerOffset = layerTileOffsets.remove(first);
		layerTileOffsets.insertElementAt(
				layerTileOffsets.elementAt(second - 1), first);
		layerTileOffsets.insertElementAt(layerOffset, second);
	}

	@Override
	public void removeLayer(int layerNum) {
		if (mapLayers.size() > 0)
			if (layerNum < mapLayers.size() && layerNum >= 0) {
				mapLayers.remove(layerNum);
				layerTileOffsets.remove(layerNum);
			}
	}

	/**
	 * Use this method to get all the layers of this map.
	 * 
	 * @return Vector<Object> - all the layers in this map.
	 */
	public Vector<Object> getLayers() {
		return mapLayers;
	}

	/**
	 * Use this method to get all the tilebased layers of this map.
	 * 
	 * @return Vector<int[][]> - all the layers in this map.
	 */
	public Vector<TiledLayer> getTileLayers() {

		Vector<TiledLayer> layers = new Vector<TiledLayer>();
		for (Object o : mapLayers)
			layers.add((TiledLayer) o);
		return layers;
	}

	/**
	 * Use to change a tile on the map.
	 * 
	 * @param x
	 *            The horizontal position, in tiles, of the tile to be changed.
	 * @param y
	 *            The vertical position, in tiles, of the tile to be changed.
	 * @param layerNum
	 *            The index of the layer to be changed.
	 */
	public void setTile(int x, int y, int layerNum, boolean undo) {
		if (getLayer(layerNum) instanceof TiledLayer) {
			if ((x >= 0 && x < Settings.CurrentMapWidth)
					&& (y >= 0 && y < Settings.CurrentMapHeight)) {
				if (!undo
						&& ((TiledLayer) getLayer(layerNum)).getLayer()[y][x] != currentTex)
					changeHandler.done(x, y,
							((TiledLayer) getLayer(layerNum)).getLayer()[y][x],
							currentTex, layerNum);
				((TiledLayer) getLayer(layerNum)).setTile(x, y, currentTex);
			}
		}
	}

	/**
	 * Use to fill all tiles that are like the current texture of the tile that
	 * is first indicated with the current texturevalue.
	 * 
	 * @param x
	 *            The horizontal position, in tiles, of the origin tile.
	 * @param y
	 *            The vertical position, in tiles, of the origin tile.
	 * @param layerNum
	 *            The index of the layer to be changed.
	 */
	public void floodFill(int x, int y, int layerNum) {
		boolean[][] checked = new boolean[Settings.CurrentMapHeight][Settings.CurrentMapWidth];

		Vector<Integer> toBeTestedX = new Vector<Integer>();
		Vector<Integer> toBeTestedY = new Vector<Integer>();

		int compareTex = ((TiledLayer) getLayer(layerNum)).getLayer()[y
				/ Settings.CurrentTileHeight][x / Settings.CurrentTileWidth];

		toBeTestedX.add(x / Settings.CurrentMapWidth);
		toBeTestedY.add(y / Settings.CurrentMapHeight);
		checked[y][x] = true;

		if (getCurrentTex() == compareTex)
			return;

		while (toBeTestedX.size() > 0) {
			if (compareTex == ((TiledLayer) getLayer(layerNum)).getLayer()[toBeTestedY
					.get(0)][toBeTestedX.get(0)]) {
				setTile(toBeTestedX.get(0), toBeTestedY.get(0), layerNum, false);
			} else {
				toBeTestedX.remove(0);
				toBeTestedY.remove(0);
				continue;
			}

			// LEFT
			if (toBeTestedX.get(0) - 1 >= 0)
				if (!checked[toBeTestedY.get(0)][toBeTestedX.get(0) - 1] == true
						&& compareTex == ((TiledLayer) getLayer(layerNum))
								.getLayer()[toBeTestedY.get(0)][toBeTestedX
								.get(0) - 1]) {
					toBeTestedX.add(toBeTestedX.get(0) - 1);
					toBeTestedY.add(toBeTestedY.get(0));
				}
			// RIGHT
			if (toBeTestedX.get(0) < Settings.CurrentMapWidth - 1)
				if (!checked[toBeTestedY.get(0)][toBeTestedX.get(0) + 1] == true
						&& compareTex == ((TiledLayer) getLayer(layerNum))
								.getLayer()[toBeTestedY.get(0)][toBeTestedX
								.get(0) + 1]) {
					toBeTestedX.add(toBeTestedX.get(0) + 1);
					toBeTestedY.add(toBeTestedY.get(0));
				}
			// TOP
			if (toBeTestedY.get(0) - 1 >= 0)
				if (!checked[toBeTestedY.get(0) - 1][toBeTestedX.get(0)] == true
						&& compareTex == ((TiledLayer) getLayer(layerNum))
								.getLayer()[toBeTestedY.get(0) - 1][toBeTestedX
								.get(0)]) {
					toBeTestedX.add(toBeTestedX.get(0));
					toBeTestedY.add(toBeTestedY.get(0) - 1);
				}
			// BOTTOM
			if (toBeTestedY.get(0) < Settings.CurrentMapHeight - 1)
				if (!checked[toBeTestedY.get(0) + 1][toBeTestedX.get(0)] == true
						&& compareTex == ((TiledLayer) getLayer(layerNum))
								.getLayer()[toBeTestedY.get(0) + 1][toBeTestedX
								.get(0)]) {
					toBeTestedX.add(toBeTestedX.get(0));
					toBeTestedY.add(toBeTestedY.get(0) + 1);
				}
			checked[toBeTestedY.remove(0)][toBeTestedX.remove(0)] = true;
		}
	}

}
