package logic;

import java.io.Serializable;

public class Layer implements Serializable {
	private static final long serialVersionUID = 1342901455920438877L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Layer() {
		this.name = "";
	}

	public Layer(String name) {
		this.name = name;
	}
}
