package logic;

import java.io.Serializable;
import java.util.Vector;

/**
 * The ChangeHandler class is responsible for keeping track of changes that are
 * made and redoes or undoes them as necessary.
 */
public class ChangeHandler implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6896694790187596567L;
	private static int MAXSTORED = 50;
	private Vector<int[]> done;
	private Vector<int[]> undone;

	/**
	 * Constructor.
	 */
	public ChangeHandler() {
		done = new Vector<int[]>();
		undone = new Vector<int[]>();
	}

	/**
	 * Use this method to register changes that are done. They will be added to
	 * the Vector<int[]> as the last change made.
	 * 
	 * @param x
	 *            - Horizontal number of the tile changed.
	 * @param y
	 *            - Vertical number of the tile changed.
	 * @param oldTex
	 *            - The number of the texture-tile that was changed from.
	 * @param newTex
	 *            - The number of the texture-tile that is changed to.
	 * @param layerNum
	 *            - The number of the layer that was changed.
	 */
	public void done(int x, int y, int oldTex, int newTex, int layerNum) {
		int[] a = new int[5];
		a[0] = x;
		a[1] = y;
		a[2] = oldTex;
		a[3] = newTex;
		a[4] = layerNum;

		done.add(a);
		if (done.size() > MAXSTORED)
			done.remove(0);
	}

	/**
	 * Use this method to undo the last change added to the done vector,
	 * recognized as the most recent change made to the map.
	 * 
	 * @return int[] array containing; 0. x-value, 1. y-value, 2. number of
	 *         old-texture, 3. the number of the layer.
	 */
	public int[] undo() {
		if (done.size() > 0) {
			int[] a = new int[4];

			a = new int[4];
			a[0] = done.lastElement()[0];
			a[1] = done.lastElement()[1];
			a[2] = done.lastElement()[2];
			a[3] = done.lastElement()[4];
			;

			undone.add(done.lastElement());
			done.remove(done.lastElement());
			if (undone.size() > MAXSTORED)
				undone.remove(0);
			return a;
		}
		return null;
	}

	/**
	 * Use this method to redo the last change removed from the done vector,
	 * recognized as the most recent change that was undone.
	 * 
	 * @return int[] array containing; 0. x-value, 1. y-value, 2. number of
	 *         new-texture, 3. the number of the layer.
	 */
	public int[] redo() {
		if (undone.size() > 0) {

			int[] a = new int[4];
			a[0] = undone.lastElement()[0];
			a[1] = undone.lastElement()[1];
			a[2] = undone.lastElement()[3];
			a[3] = undone.lastElement()[4];

			done.add(undone.lastElement());
			undone.remove(done.lastElement());
			return a;
		}
		return null;
	}
}
