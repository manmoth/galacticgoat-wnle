package logic;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import logic.MapManager.MapType;

/**
 * The HybridMap class extends the {@link TiledMap} class to add support for
 * polygon layers.
 */
public class HybridMap extends TiledMap {
	private static final long serialVersionUID = -4760133625916241346L;

	private Map<String, Vector<File>> spriteSets;

	public Map<String, Vector<File>> getSpriteSets() {
		return spriteSets;
	}

	public void setSpriteSets(Map<String, Vector<File>> spriteSets) {
		this.spriteSets = spriteSets;
	}

	@Override
	public MapType getType() {
		return type;
	}

	private int currentSpriteTex = 0;

	/**
	 * @return The index of the texture to be used when adding sprites.
	 */
	public int getCurrentSpriteTex() {
		return currentSpriteTex;
	}

	/**
	 * @param currentSprite
	 *            The index of the texture to be used when adding sprites.
	 */
	public void setCurrentSpriteTex(int currentSpriteTex) {
		this.currentSpriteTex = currentSpriteTex;
	}

	/**
	 * Constructor. Will instantiate the layer-vector.
	 */
	public HybridMap(int mapWidth, int mapHeight, int tileWidth, int tileHeight) {
		super(mapWidth, mapHeight, tileWidth, tileHeight);
		type = MapType.HYBRID;
		mapLayers = new Vector<Object>();

		spriteSets = new HashMap<String, Vector<File>>(0);
	}

	/**
	 * Gets the {@link PolygonLayer} at the specified index.
	 * 
	 * @param layerNum
	 * @return {@link PolygonLayer} at specified index or null if no such layer
	 *         exists
	 */
	public PolygonLayer getPolyLayer(int layerNum) {
		if (mapLayers.get(layerNum) instanceof PolygonLayer)
			return (PolygonLayer) mapLayers.get(layerNum);
		else
			return null;
	}

	/**
	 * Adds a new {@link PolygonLayer} to the map.
	 */
	public void addPolyLayer(String name) {
		mapLayers.add(new PolygonLayer(name));
		int[] tileOffset = { 0, -1 };
		layerTileOffsets.add(tileOffset);
	}

	/**
	 * Adds a new {@link SpriteLayer} to the map.
	 */
	public void addSpriteLayer(String name, String spriteSetId) {
		mapLayers.add(new SpriteLayer(name, spriteSetId));
		int[] tileOffset = { 0, -1 };
		layerTileOffsets.add(tileOffset);
	}

	@Override
	public void removeLayer(int layerNum) {
		if (mapLayers.size() > 0)
			if (layerNum < mapLayers.size() && layerNum >= 0) {
				mapLayers.remove(layerNum);
			}
	}

	@Override
	public Vector<Object> getLayers() {
		return mapLayers;
	}

	/**
	 * Use this method to get all the tilebased layers of this map.
	 * 
	 * @return Vector<TiledLayer> - all the layers in this map.
	 */
	public Vector<TiledLayer> getTileLayers() {
		Vector<TiledLayer> layers = new Vector<TiledLayer>();

		for (Object o : mapLayers)
			if (o instanceof TiledLayer)
				layers.add((TiledLayer) o);

		return layers;
	}

	/**
	 * Use this method to get all the polygonbased layers of this map.
	 * 
	 * @return Vector<PolygonLayer> - all the layers in this map.
	 */
	public Vector<PolygonLayer> getPolyLayers() {
		Vector<PolygonLayer> layers = new Vector<PolygonLayer>();

		for (Object o : mapLayers)
			if (o instanceof PolygonLayer)
				layers.add((PolygonLayer) o);
		return layers;
	}

	/**
	 * Use this method to get all the spritebased layers of this map.
	 * 
	 * @return Vector<SpriteLayer> - all the layers in this map.
	 */
	public Vector<SpriteLayer> getSpriteLayers() {
		Vector<SpriteLayer> layers = new Vector<SpriteLayer>();

		for (Object o : mapLayers)
			if (o instanceof SpriteLayer)
				layers.add((SpriteLayer) o);
		return layers;
	}
}
