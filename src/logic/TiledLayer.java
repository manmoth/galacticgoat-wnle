package logic;

import java.io.File;
import java.io.Serializable;

/**
 * The TiledLayer class is a wrapper around a int[][] used to define a Layer
 * object.
 */
public class TiledLayer extends Layer implements Serializable {

	private String tileSetId;

	public String getTileSetId() {
		return tileSetId;
	}

	public void setTileSetId(String tileSetId) {
		this.tileSetId = tileSetId;
	}

	private static final long serialVersionUID = 5132826864567866777L;
	private int[][] layer;

	public TiledLayer(String name, String tileSetId, int mapWidth, int mapHeight) {
		super(name);
		this.tileSetId = tileSetId;
		layer = new int[mapHeight][mapWidth];
	}

	public TiledLayer(String name, String tileSetId, int[][] layer) {
		super(name);
		this.tileSetId = tileSetId;
		this.layer = layer;
	}

	public void setTile(int x, int y, int val) {
		layer[y][x] = val;
	}

	public int[][] getLayer() {
		return layer;
	}
}
