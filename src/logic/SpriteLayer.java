package logic;

import java.awt.Point;
import java.io.File;
import java.io.Serializable;
import java.util.Vector;

public class SpriteLayer extends Layer implements Serializable {

	private static final long serialVersionUID = -8632733147652046303L;

	private String spriteSetId;

	public String getSpriteSetId() {
		return spriteSetId;
	}

	public void setSpriteSetId(String spriteSetId) {
		this.spriteSetId = spriteSetId;
	}

	private Vector<Sprite> sprites;

	public SpriteLayer(String name, String spriteSetId) {
		super(name);
		this.spriteSetId = spriteSetId;
		sprites = new Vector<Sprite>();
	}

	public boolean addSprite(Point pos, int val, String name) {
		return sprites.add(new Sprite(val, pos, name));
	}

	public Sprite getSprite(int spriteIndex) {
		if (spriteIndex < sprites.size() && spriteIndex >= 0)
			return sprites.elementAt(spriteIndex);
		else
			return null;
	}

	public Vector<Sprite> getSprites() {
		return sprites;
	}

	public Sprite removeSprite(int spriteIndex) {
		if (spriteIndex < sprites.size() && spriteIndex >= 0)
			return sprites.remove(spriteIndex);
		else
			return null;
	}

	public int size() {
		return sprites.size();
	}

	public int indexOf(Vector<Point> p) {
		return sprites.indexOf(p);
	}
}
