package logic;

import java.awt.Point;
import java.io.Serializable;

public class Sprite implements Serializable {
	private static final long serialVersionUID = -1013007169320790243L;
	private int val;
	private String name;
	private Point pos;

	public int getVal() {
		return val;
	}

	public String getName() {
		return name;
	}

	public Point getPos() {
		return pos;
	}

	public Sprite(int val, Point pos, String name) {
		this.val = val;
		this.pos = pos;
		this.name = name;
	}
}
