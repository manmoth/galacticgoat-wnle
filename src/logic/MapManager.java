package logic;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import view.Settings;
import view.ViewController;
import data.MapExporter;
import data.MapExporter.IOType;
import data.ImageLoader;
import data.MapImporter;
import data.ScriptInterpreter;
import data.SpriteSet;
import data.TileSet;

/**
 * The MapManager is used to update and reference the current map. It is also
 * responsible for loading and saving the map.
 */
public class MapManager {

	ViewController viewController;

	/**
	 * This enum is used to specify the type of a map.
	 */
	public enum MapType {
		TILED, HYBRID;
	}

	MapInterface currentMap;

	/**
	 * @return The index of the current layer in the map.
	 */
	public int getCurrentLayerIndex() {
		if (currentMap != null) {
			for (int i = 0; i < ((TiledMap) (getMap())).getLayers().size(); i++) {
				if (((TiledMap) (getMap())).getLayers().get(i) == currentLayer)
					return i;
			}
		}
		return 0;
	}

	/**
	 * @return The index of the current polygon in the layer vector.
	 */
	public int getCurrentPolyIndex() {
		if (currentMap != null && getLayer() instanceof PolygonLayer) {
			for (PolygonLayer v : ((HybridMap) (getMap())).getPolyLayers()) {
				for (int i = 0; i < v.size(); i++) {
					if (v.getPolygon(i) == currentPoly)
						return i;
				}
			}
		}
		return 0;
	}

	/**
	 * @return The index of the current sprite in the layer vector.
	 */
	public int getCurrentSpriteIndex() {
		if (currentMap != null && getLayer() instanceof SpriteLayer) {
			for (SpriteLayer v : ((HybridMap) (getMap())).getSpriteLayers()) {
				for (int i = 0; i < v.size(); i++) {
					if (v.getSprite(i) == currentSprite)
						return i;
				}
			}
		}
		return 0;
	}

	Object currentLayer;
	Object currentPoly;
	Object currentSprite;

	/**
	 * Returns the active map.
	 * 
	 * @return {@Link MapInterface} object, use map.getType to cast to
	 *         the specific map-class.
	 */
	public MapInterface getMap() {
		return currentMap;
	}

	/**
	 * Use to get the active layer.
	 * 
	 * @return Object representing the current layer.
	 */
	public Object getLayer() {
		return currentLayer;
	}

	/**
	 * Use to set the active layer.
	 * 
	 * @param layerNum
	 *            The index of the layer to be used.
	 */
	public void setLayer(int layerNum) {
		if (layerNum < currentMap.getLayers().size() && layerNum >= 0)
			currentLayer = currentMap.getLayer(layerNum);
	}

	/**
	 * Use to set the active polygon.
	 * 
	 * @param index
	 *            of the polygon to be used.
	 */
	public void setPoly(int polygonNum) {
		if (getLayer() instanceof PolygonLayer) {
			if (polygonNum < ((PolygonLayer) getLayer()).size()
					&& polygonNum >= 0)
				currentPoly = ((PolygonLayer) getLayer())
						.getPolygon(polygonNum);
		}
	}

	/**
	 * Use to set the active sprite.
	 * 
	 * @param spriteNum
	 *            The index of the sprite to be used.
	 */
	public void setSprite(int spriteNum) {
		if (getLayer() instanceof SpriteLayer) {
			if (spriteNum < ((SpriteLayer) getLayer()).size() && spriteNum >= 0)
				currentSprite = ((SpriteLayer) getLayer()).getSprite(spriteNum);
		}
	}

	/**
	 * Constructor.
	 */
	public MapManager(ViewController viewController) {
		this.viewController = viewController;
	}

	/**
	 * Use to create a new hybrid map.
	 * 
	 * @param mapWidth
	 *            The width, in tiles, of the map to be created.
	 * @param mapHeight
	 *            The height, in tiles, of the map to be created.
	 * @param tileWidth
	 *            The width, in pixels, of each map-tile.
	 * @param tileHeight
	 *            The height, in pixels, of each map-tile.
	 */
	public void newHybridMap(int mapWidth, int mapHeight, int tileWidth,
			int tileHeight) {

		Settings.CurrentMapWidth = mapWidth;
		Settings.CurrentMapHeight = mapHeight;
		Settings.CurrentTileWidth = tileWidth;
		Settings.CurrentTileHeight = tileHeight;
		currentMap = new HybridMap(mapWidth, mapHeight, tileWidth, tileHeight);
	}

	/**
	 * Method used to optimally resize the map. It will check all sides for
	 * empty areas and stop at the first column/row with a non-zero tile.
	 * 
	 * @param mapWidth
	 *            - The width in tiles of the resized map.
	 * @param mapHeight
	 *            - The height in tiles of the resized map.
	 */
	public void optimalResize() {
		if (currentMap == null)
			return;
		int farRight = Settings.CurrentMapWidth / 2;
		int farLeft = farRight;
		int top = Settings.CurrentMapHeight / 2;
		int bottom = top;

		for (Object l : currentMap.getLayers()) {
			if (l instanceof TiledLayer) {
				for (int y = 0; y < Settings.CurrentMapHeight; y++) {
					for (int x = 0; x < Settings.CurrentMapWidth; x++) {
						if (((TiledLayer) l).getLayer()[y][x] != 0) {
							if (y < top)
								top = y;
							if (y > bottom)
								bottom = y;
							if (x < farLeft)
								farLeft = x;
							if (x > farRight)
								farRight = x;
						}
					}
				}
			}
		}
		for (Object l : currentMap.getLayers()) {
			if (l instanceof TiledLayer) {

				int[][] layer = new int[bottom - top + 1][farRight - farLeft
						+ 1];

				for (int y = top; y < bottom + 1; y++) {
					for (int x = farLeft; x < farRight + 1; x++) {
						layer[y - top][x - farLeft] = ((TiledLayer) l)
								.getLayer()[y][x];
					}
				}
				currentMap.setLayer(new TiledLayer(((TiledLayer) l).getName(),
						((TiledLayer) l).getTileSetId(), layer), currentMap
						.getLayers().indexOf(l));
			}
		}
		Settings.CurrentMapHeight = ((TiledLayer) ((TiledMap) currentMap)
				.getLayer(0)).getLayer().length;
		((TiledMap) currentMap)
				.setMapHeight(((TiledLayer) ((TiledMap) currentMap).getLayer(0))
						.getLayer().length);
		Settings.CurrentMapWidth = ((TiledLayer) ((TiledMap) currentMap)
				.getLayer(0)).getLayer()[0].length;
		((TiledMap) currentMap)
				.setMapWidth(((TiledLayer) ((TiledMap) currentMap).getLayer(0))
						.getLayer()[0].length);
	}

	/**
	 * Method used to resize the map to a specific size. Will cut off any areas
	 * outside the new specified size if it is smaller.
	 * 
	 * @param mapWidth
	 *            - The width in tiles of the resized map.
	 * @param mapHeight
	 *            - The height in tiles of the resized map.
	 */
	public void resizeMap(int mapWidth, int mapHeight, int left, int right,
			int top, int bottom) {
		if (currentMap == null)
			return;
		if (top + mapHeight + bottom < -1 || left + mapWidth + right < -1)
			return;

		for (Object l : currentMap.getLayers()) {
			if (l instanceof TiledLayer) {
				int[][] layer = new int[top
						+ (mapHeight > 0 ? mapHeight
								: Settings.CurrentMapHeight) + bottom][left
						+ (mapWidth > 0 ? mapWidth : Settings.CurrentMapWidth)
						+ right];

				int mw = mapWidth;
				int mh = mapHeight;

				if (mh < 0 || mapWidth >= Settings.CurrentMapWidth)
					mw = Settings.CurrentMapWidth;
				if (mh < 0 || mapHeight >= Settings.CurrentMapHeight)
					mh = Settings.CurrentMapHeight;

				for (int y = 0; y < mh; y++) {
					for (int x = 0; x < mw; x++) {
						layer[top + y][left + x] = ((TiledLayer) l).getLayer()[y][x];
					}
				}
				currentMap.setLayer(new TiledLayer(((TiledLayer) l).getName(),
						((TiledLayer) l).getTileSetId(), layer), currentMap
						.getLayers().indexOf(l));
			}
		}
		Settings.CurrentMapHeight = ((TiledLayer) ((TiledMap) currentMap)
				.getLayer(0)).getLayer().length;
		((TiledMap) currentMap)
				.setMapHeight(((TiledLayer) ((TiledMap) currentMap).getLayer(0))
						.getLayer().length);
		Settings.CurrentMapWidth = ((TiledLayer) ((TiledMap) currentMap)
				.getLayer(0)).getLayer()[0].length;
		((TiledMap) currentMap)
				.setMapWidth(((TiledLayer) ((TiledMap) currentMap).getLayer(0))
						.getLayer()[0].length);

	}

	private File currentDirectory = null;

	public void SetWorkingDirectory(String path) {
		currentDirectory = new File(path);
	}

	/**
	 * Use to export the current map to a format specified by an IOType enum.
	 * 
	 * @param et
	 *            The type of the exported file.
	 */
	public void exportMap(IOType et, int scriptIndex,
			Vector<BufferedImage> tileSet) {
		if (currentMap == null)
			return;
		switch (et) {
		case PNG:
			MapExporter.exportToPNG(currentMap, tileSet);
			break;
		case TXT:
			MapExporter.exportToTXT(currentMap);
			break;
		case XML:
			MapExporter.exportToXML(currentMap);
			break;
		case JSON:
			MapExporter.exportToJSON(currentMap);
			break;
		case USER:
			MapExporter.exportToXML(
					ScriptInterpreter.getAvailableExportScripts()[scriptIndex],
					currentMap);
			break;
		}
	}

	/**
	 * Use to import a map from a format specified by an IOType enum.
	 * 
	 * @param et
	 *            The type of the file to be imported.
	 */
	public void importMap(IOType it) {
		switch (it) {
		case TXT:
			MapImporter.importFromTXT();
			break;
		case XML:
			MapImporter.importFromXML();
			break;
		}
	}

	/**
	 * Use to save the current map to a specific file. Will use
	 * objectoutputstream to serialize the map.
	 */
	public void saveMap() {

		if (currentMap == null)
			return;

		JFileChooser jfc = new JFileChooser();
		jfc.setCurrentDirectory(currentDirectory);
		int returnVal = jfc.showSaveDialog(null);
		currentDirectory = jfc.getCurrentDirectory();
		if (returnVal == JFileChooser.APPROVE_OPTION)
			try {
				FileOutputStream f_out = new FileOutputStream(jfc
						.getSelectedFile().getAbsolutePath());

				ObjectOutputStream obj_out = new ObjectOutputStream(f_out);

				obj_out.writeObject(currentMap);

			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	/**
	 * Use to open a saved map from a specific file. Will also request the
	 * location of a tileset to be used with the map.
	 */
	public void openMap(HashMap<String, TileSet> tileSets,
			HashMap<String, SpriteSet> spriteSets) {
		JFileChooser jfc = new JFileChooser();
		jfc.setCurrentDirectory(currentDirectory);
		int returnVal = jfc.showOpenDialog(null);
		currentDirectory = jfc.getCurrentDirectory();
		if (returnVal == JFileChooser.APPROVE_OPTION)
			try {
				FileInputStream f_in = new FileInputStream(
						jfc.getSelectedFile());

				ObjectInputStream obj_in = new ObjectInputStream(f_in);

				Object obj = obj_in.readObject();

				if (obj instanceof MapInterface) {

					currentMap = (MapInterface) obj;

					Settings.CurrentMapWidth = currentMap.getMapWidth();
					Settings.CurrentMapHeight = currentMap.getMapHeight();
					if (currentMap instanceof TiledMap) {
						Settings.CurrentTileWidth = ((TiledMap) currentMap)
								.getTileWidth();
						Settings.CurrentTileHeight = ((TiledMap) currentMap)
								.getTileHeight();
					}
					for (String name : ((TiledMap) currentMap).getTileSets()
							.keySet()) {
						TileSet ts = ImageLoader
								.loadTileset(name, ((TiledMap) currentMap)
										.getTileSets().get(name));
						tileSets.put(name, ts);
					}
					if (currentMap instanceof HybridMap)
						for (String name : ((HybridMap) currentMap)
								.getSpriteSets().keySet()) {
							SpriteSet ss = ImageLoader.loadSpriteSet(name,
									((HybridMap) currentMap).getSpriteSets()
											.get(name));
							spriteSets.put(name, ss);
						}
					setLayer(0);

					obj_in.close();

				}
			} catch (Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,
						"Wrong format or old version",
						JOptionPane.MESSAGE_PROPERTY,
						JOptionPane.WARNING_MESSAGE);
			}
	}
}
