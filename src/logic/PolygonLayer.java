package logic;

import java.awt.Point;
import java.io.Serializable;
import java.util.Vector;

/**
 * The PolygonLayer class is a wrapper around a Vector<Vector<Point>> used to
 * avoid unchecked casting.
 */
public class PolygonLayer extends Layer implements Serializable {

	private static final long serialVersionUID = -2305886062652293791L;
	private Vector<Vector<Point>> polygons;

	public PolygonLayer(String name) {
		super(name);
		polygons = new Vector<Vector<Point>>();
	}

	public boolean addPolygon(Vector<Point> polygon) {
		return polygons.add(polygon);
	}

	public Vector<Point> getPolygon(int polyIndex) {
		if (polyIndex < polygons.size() && polyIndex >= 0)
			return polygons.elementAt(polyIndex);
		else
			return null;
	}

	public Vector<Vector<Point>> getPolygons() {
		return polygons;
	}

	public Vector<Point> removePolygon(int polyIndex) {
		if (polyIndex < polygons.size() && polyIndex >= 0)
			return polygons.remove(polyIndex);
		else
			return null;
	}

	public int size() {
		return polygons.size();
	}

	public int indexOf(Vector<Point> p) {
		return polygons.indexOf(p);
	}
}