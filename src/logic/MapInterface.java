package logic;

import java.util.Vector;

import logic.MapManager.MapType;

/**
 * This interface describes the basic methods of a standard-map, as used by the
 * editor.
 */
public interface MapInterface {

	/**
	 * Use this method to cast to the specific map-class.
	 * 
	 * @return {@link MapType} of the map-object.
	 */
	public MapType getType();

	/**
	 * Use this method to add a new layer.
	 */
	public void addLayer(String name, String id);

	/**
	 * Use this method to swap two layers.
	 * 
	 * @param first
	 *            - the index of the first layer to be swapped.
	 * @param second
	 *            - the index of the second layer to be swapped.
	 */
	public void swapLayers(int first, int second);

	/**
	 * Use this method to remove a specific layer.
	 * 
	 * @param layerNum
	 *            - index of the layer to be removed.
	 */
	public void removeLayer(int layerNum);

	/**
	 * Use this method to get the layers of this map.
	 * 
	 * @return A Vector<Object> containing the layers of this map.
	 */
	public Vector<Object> getLayers();

	/**
	 * Use this method to get a specific layer.
	 * 
	 * @param layerNum
	 *            The index of the layer to be selected.
	 * 
	 * @return Object at the index of the layer-vector.
	 */
	public Object getLayer(int layerNum);

	public void setLayer(Object layer, int index);

	public int getMapWidth();

	public int getMapHeight();

	public void setMapWidth(int width);

	public void setMapHeight(int height);
}
